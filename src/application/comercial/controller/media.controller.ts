import { Controller, Get, Param, Res } from '@nestjs/common'
import { Response } from 'express'
import { BaseController } from '../../../common/base/base-controller'
import { UtilService } from '../../../common/lib/util.service'

@Controller('uploads')
export class UploadsController extends BaseController {
  @Get(':filename')
  async obtenerArchivo(
    @Param('filename') filename: string,
    @Res() res: Response
  ) {
    const path = 'uploads/' + filename
    //devolvemos el archivo sin utilizar el servicio de utilidades
    return
    //return UtilService.obtenerArchivo(path, res)
  }
}
