import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { SeguimientoRepository } from '../repository/seguimiento.repository'

@Injectable()
export class SeguimientoService extends BaseService {
  constructor(
    @Inject(SeguimientoRepository)
    private seguimientoRepositorio: SeguimientoRepository
  ) {
    super()
  }

  async crear(seguimientoDto: any, usuarioAuditoria: string) {
    return await this.seguimientoRepositorio.crear(
      seguimientoDto,
      usuarioAuditoria
    )
  }

  async actualizar(id: string, seguimientoDto: any, usuarioAuditoria: string) {
    return await this.seguimientoRepositorio.actualizar(
      id,
      seguimientoDto,
      usuarioAuditoria
    )
  }
  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.seguimientoRepositorio.listar(paginacionQueryDto)
  }
}
