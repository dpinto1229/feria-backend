import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable } from '@nestjs/common'
import { DashboardRepository } from '../repository/dashboard.reporsitory'

@Injectable()
export class DashboardService extends BaseService {
  constructor(
    @Inject(DashboardRepository)
    private dahsboardRepositorio: DashboardRepository
  ) {
    super()
  }
  async totalEmpresas() {
    const empresas = await this.dahsboardRepositorio.listarTotalEmpresas()
    return {
      numero: empresas,
      titulo: 'Empresas',
      color: '#4e73df',
      icon: 'apartment',
      colorIcono: '#fff',
    }
  }

  async totalUsuarios() {
    const usuarios = await this.dahsboardRepositorio.listarTotalUsuarios()
    return {
      numero: usuarios,
      titulo: 'Usuarios',
      color: '#1cc88a',
      icon: 'people',
      colorIcono: '#fff',
    }
  }

  async totalDocumentos() {
    const documentos = await this.dahsboardRepositorio.listarTotalDocumentos()
    return {
      numero: documentos,
      titulo: 'Documentos',
      color: '#36b9cc',
      icon: 'description',
      colorIcono: '#fff',
    }
  }

  async cards() {
    const respuestas: any[] = []
    respuestas.push(await this.totalEmpresas())
    respuestas.push(await this.totalUsuarios())
    respuestas.push(await this.totalDocumentos())
    return respuestas
  }

  async charts() {
    const documentosEmpresa =
      await this.dahsboardRepositorio.listarDocumentosPorEmpresa()
    const armarData = await this.armarChart(documentosEmpresa)
    return armarData
  }

  private async armarChart(datos: any[]) {
    const data: Array<string>[] = []

    data.push(['Nombre Empresa', 'Cantidad de documentos'])
    datos.forEach((element: any) => {
      data.push([element.empresa.nombreEmpresa, data.length])
    })
    console.log(datos)
    return data
  }
}
