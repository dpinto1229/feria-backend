import { MigrationInterface, QueryRunner } from 'typeorm'
import { USUARIO_SISTEMA } from '../../src/common/constants'
import { Plantas } from 'src/application/comercial/entities/plantas.entity'

export class insertsPlantas1617820337611 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const items = [
      {
        nivel: 'Planta Alta',
        descripcion: 'Planta Alta del Bloque Amarillo',
        idBloque: '1',
        imagen: 'https://i.imgur.com/WNd4TtB.png',
        rutaModelo:
          'https://home.by.me/es/project/kevincho10916038-2218/expo36-2024?open_planner=true',
        estado: 'ACTIVO',
      },
      {
        nivel: 'Planta Baja',
        descripcion: 'Planta Baja del Bloque Amarillo',
        idBloque: '1',
        imagen: 'https://i.imgur.com/tIPMmXk.png',
        rutaModelo:
          'https://home.by.me/es/project/kevincho10916038-2218/amarillo-bajo?open_planner=true',
        estado: 'ACTIVO',
      },
      {
        nivel: 'Planta Alta',
        descripcion: 'Planta Alta del Bloque Verde',
        idBloque: '2',
        estado: 'ACTIVO',
      },
      {
        nivel: 'Planta Baja',
        descripcion: 'Planta Baja del Bloque Verde',
        idBloque: '2',
        estado: 'ACTIVO',
      },
      {
        nivel: 'Planta Alta',
        descripcion: 'Planta Alta del Bloque Rojo',
        idBloque: '3',
        imagen: 'https://i.imgur.com/P9CdU0j.png',
        rutaModelo:
          'https://home.by.me/es/project/kevincho10916038-2218/rojo-alto?open_planner=true',
        estado: 'ACTIVO',
      },
      {
        nivel: 'Planta Baja',
        descripcion: 'Planta Baja del Bloque Rojo',
        idBloque: '3',
        rutaModelo:
          'https://home.by.me/es/project/kevincho10916038-2218/expo?open_planner=true',
        imagen: 'https://i.imgur.com/E6toMMe.png',
        estado: 'ACTIVO',
      },
    ]
    const plantas = items.map((item) => {
      return new Plantas({
        nivel: item.nivel,
        descripcion: item.descripcion,
        idBloque: item.idBloque,
        imagen: item.imagen,
        rutaModelo: item.rutaModelo,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(plantas)
  }

  /* eslint-disable */
  public async down(queryRunner: QueryRunner): Promise<void> {}
} 