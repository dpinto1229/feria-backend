export class ReservasDto {
  monto: number
  idEmpresa: string
  idStand: string
  idIngreso?: string
  descuento?: number
}
