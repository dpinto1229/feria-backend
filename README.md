## Descripción

Proyecto para la reserva de stands con NestJS y TypeORM.

## Installación de dependencias

```bash
$ npm install
```
## Configuración del proyecto
```bash
Paso 1: Copiar el archivo .env.example a .env

cp .env.example .env

Paso 2: Crear schema de la base de datos

create schema proyecto;
create schema usuarios;
create schema parametricas;
create schema feria;

Paso 3: Ejecutar el comando para las migraciones

npm run setup

```
## Ejecución del proyecto

```bash
# modo producción
$ npm run start

# modo desarrollo
$ npm run dev

# modo desarrollo
$ npm run start:dev
```

## License

Nest is [MIT licensed](LICENSE).
