import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Empresas } from '../entities/empresas.entity'
import { CrearActualizarEmpresaDto } from '../dto/empresaDto'

@Injectable()
export class EmpresaRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Empresas)
      .createQueryBuilder('empresas')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    empresaDto: CrearActualizarEmpresaDto,
    usuarioAuditoria: string
  ) {
    const datosActualizar = new Empresas({
      ...empresaDto,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Empresas)
      .update(id, datosActualizar)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro, orden, sentido } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Empresas)
      .createQueryBuilder('empresas')
      .take(limite)
      .skip(saltar)

    switch (orden) {
      case 'nombreEmpresa':
        query.addOrderBy('empresas.nombreEmpresa', sentido)
        break
      case 'descripcionEmpresa':
        query.addOrderBy('empresas.descripcionEmpresa', sentido)
        break
      case 'rubroEmpresa':
        query.addOrderBy('empresas.rubroEmpresa', sentido)
        break
      case 'estado':
        query.addOrderBy('empresas.evento', sentido)
        break
      default:
        query.orderBy('empresas.id', 'ASC')
    }

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('empresas.evento ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }

  async crear(empresaDto: CrearActualizarEmpresaDto, usuarioAuditoria: string) {
    const {
      nombreEmpresa,
      correoEmpresa,
      descripcionEmpresa,
      nombreEncargado,
      rubroEmpresa,
      telefonoEmpresa,
      direccionEmpresa,
      nitEmpresa,
      evento,
    } = empresaDto

    const empresa = new Empresas()
    empresa.nombreEmpresa = nombreEmpresa
    empresa.correoEmpresa = correoEmpresa
    empresa.descripcionEmpresa = descripcionEmpresa
    empresa.nombreEncargado = nombreEncargado
    empresa.rubroEmpresa = rubroEmpresa
    empresa.telefonoEmpresa = telefonoEmpresa
    empresa.usuarioCreacion = usuarioAuditoria
    empresa.direccionEmpresa = direccionEmpresa
    empresa.evento = evento
    empresa.nitEmpresa = nitEmpresa

    return await this.dataSource.getRepository(Empresas).save(empresa)
  }
}
