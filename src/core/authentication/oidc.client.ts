import { Client } from 'openid-client'
import { custom } from 'openid-client'

//Para cambiar el timeout del cliente de ciudadanía en caso de ser necesario.
custom.setHttpOptionsDefaults({ timeout: 10000 }) // Valor por defecto: 3500

export class ClientOidcService {
  private static client: Client
}
