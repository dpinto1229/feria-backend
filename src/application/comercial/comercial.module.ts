import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Empresas } from './entities/empresas.entity'
import { EmpresaController } from './controller/empresa.controller'
import { EmpresaService } from './service/empresa.service'
import { EmpresaRepository } from './repository/empresa.repository'
import { Documento } from './entities/invitacion.entity'
import { DocumentoController } from './controller/documento.controller'
import { DocumentoService } from './service/documento.service'
import { DocumentoRepository } from './repository/documento.repository'
import { Seguimiento } from './entities/seguimiento.entity'
import { SeguimientoRepository } from './repository/seguimiento.repository'
import { SeguimientoService } from './service/seguimiento.service'
import { SeguimientoController } from './controller/seguimiento.controller'
import { EmailService } from '../mensajeria/service/mensajeria.service'
import { DashboardRepository } from './repository/dashboard.reporsitory'
import { DashboardService } from './service/dashboard.service'
import { DashboardController } from './controller/dashboard.controller'
import { Stands } from './entities/stands.entity'
import { StandsController } from './controller/stands.controller'
import { StandService } from './service/stands.service'
import { StandRepository } from './repository/stands.repository'
import { Bloques } from './entities/bloques.entity'
import { Plantas } from './entities/plantas.entity'
import { Reservas } from './entities/reserva.entity'
import { Ingresos } from './entities/ingresos.entity'
import { IngresoController } from './controller/ingresos.controller'
import { IngresosRepository } from './repository/ingresos.respositoriy'
import { IngresosService } from './service/ingresos.service'

@Module({
  controllers: [
    EmpresaController,
    DocumentoController,
    SeguimientoController,
    DashboardController,
    StandsController,
    IngresoController,
  ],
  providers: [
    EmpresaService,
    EmpresaRepository,
    DocumentoService,
    DocumentoRepository,
    SeguimientoService,
    SeguimientoRepository,
    EmailService,
    DashboardRepository,
    DashboardService,
    StandService,
    StandRepository,
    IngresosRepository,
    IngresosService,
  ],
  imports: [
    TypeOrmModule.forFeature([
      Empresas,
      Documento,
      Seguimiento,
      Stands,
      Bloques,
      Plantas,
      Reservas,
      Ingresos,
    ]),
  ],
})
export class ComercialModule {}
