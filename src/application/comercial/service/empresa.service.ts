import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Messages } from '../../../common/constants/response-messages'
import { EmpresaRepository } from '../repository/empresa.repository'
import { CrearActualizarEmpresaDto } from '../dto/empresaDto'
import { ParametroEstado } from 'src/application/parametro/constant'

@Injectable()
export class EmpresaService extends BaseService {
  constructor(
    @Inject(EmpresaRepository)
    private empresaRepositorio: EmpresaRepository
  ) {
    super()
  }

  async crear(empresaDto: CrearActualizarEmpresaDto, usuarioAuditoria: string) {
    return await this.empresaRepositorio.crear(empresaDto, usuarioAuditoria)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.empresaRepositorio.listar(paginacionQueryDto)
  }

  async actualizarDatos(
    id: string,
    empresaDto: CrearActualizarEmpresaDto,
    usuarioAuditoria: string
  ) {
    const empresa = await this.empresaRepositorio.buscarPorId(id)
    if (!empresa) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    await this.empresaRepositorio.actualizar(id, empresaDto, usuarioAuditoria)
    return { id }
  }

  async activar(idEmpresa: string, usuarioAuditoria: string) {
    const parametro = await this.empresaRepositorio.buscarPorId(idEmpresa)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const parametroDto = new CrearActualizarEmpresaDto()
    parametroDto.estado = ParametroEstado.ACTIVO
    await this.empresaRepositorio.actualizar(
      idEmpresa,
      parametroDto,
      usuarioAuditoria
    )
    return {
      id: idEmpresa,
      estado: parametroDto.estado,
    }
  }

  async inactivar(idEmpresa: string, usuarioAuditoria: string) {
    const parametro = await this.empresaRepositorio.buscarPorId(idEmpresa)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const parametroDto = new CrearActualizarEmpresaDto()
    parametroDto.estado = ParametroEstado.INACTIVO

    console.log('parametroDto', parametroDto)
    await this.empresaRepositorio.actualizar(
      idEmpresa,
      parametroDto,
      usuarioAuditoria
    )
    return {
      id: idEmpresa,
      estado: parametroDto.estado,
    }
  }
}
