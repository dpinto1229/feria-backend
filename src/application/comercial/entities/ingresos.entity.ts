import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Usuario } from 'src/core/usuario/entity/usuario.entity'
import { Reservas } from './reserva.entity'

dotenv.config()

export enum IngresosTipo {
  INGRESO = 'INGRESO',
  EGRESO = 'EGRESO',
}
@Check(UtilService.buildStatusCheck(ParametroEstado))
@Check(UtilService.buildCheck('tipo', IngresosTipo))
@Entity({ name: 'ingresos', schema: process.env.DB_SCHEMA_FERIA })
export class Ingresos extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 255,
    type: 'varchar',
    nullable: false,
    comment: 'Descripción del ingreso',
  })
  descripcion: string

  @Column({
    length: 255,
    type: 'varchar',
    nullable: false,
    comment: 'Tipo de ingreso',
  })
  tipo: string

  @Column({
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Monto del ingreso',
  })
  monto: number

  //id usuario
  @Column({
    name: 'id_usuario',
    type: 'bigint',
    nullable: false,
    comment: 'id del usuario que realizo el ingreso',
  })
  idUsuario: string

  @ManyToOne(() => Usuario, (usuario) => usuario.ingreso, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_usuario',
    referencedColumnName: 'id',
  })
  usuario: Usuario

  @OneToMany(() => Reservas, (resservas) => resservas.ingreso)
  reservas: Reservas[]
  constructor(data?: Partial<Ingresos>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ParametroEstado.ACTIVO
  }
}
