import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { Stands } from '../entities/stands.entity'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'
import { Bloques } from '../entities/bloques.entity'
import { ReservasDto } from '../dto/standsDto'
import { Reservas } from '../entities/reserva.entity'
import { Empresas } from '../entities/empresas.entity'
import { Ingresos } from '../entities/ingresos.entity'

export class CrearStandDto {
  nombre: string
  codigo: string
  area: number
  precio: number
  ubicacionX: number
  color: string
  ubicacionY: number
  idPlanta: string
  estado?: string
}
@Injectable()
export class StandRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Stands)
      .createQueryBuilder('stands')
      .where({ id: id })
      .getOne()
  }

  async BuscarEmpresaPorId(id: string) {
    return await this.dataSource
      .getRepository(Empresas)
      .createQueryBuilder('empresas')
      .where({ id: id })
      .getOne()
  }

  async listarDetalle(idBloque: string) {
    return await this.dataSource
      .getRepository(Bloques)
      .createQueryBuilder('bloques')
      .leftJoinAndSelect('bloques.plantas', 'plantas')
      .leftJoinAndSelect('plantas.stands', 'stands')
      .select([
        'bloques.id',
        'bloques.nombre',
        'bloques.descripcion',
        'bloques.imagen',
        'plantas.id',
        'plantas.nivel',
        'plantas.descripcion',
        'plantas.imagen',
        'stands.id',
        'stands.codigo',
        'stands.area',
        'stands.precio',
        'stands.ubicacionX',
        'stands.ubicacionY',
        'stands.color',
      ])
      .where('bloques.id = :idBloque', { idBloque })
      .getMany()
  }

  async listarPorPlanta(idPlanta: string) {
    return await this.dataSource
      .getRepository(Stands)
      .createQueryBuilder('stands')
      .leftJoinAndSelect('stands.plantas', 'plantas')
      .leftJoinAndSelect('stands.reservas', 'reservas')
      .leftJoinAndSelect('reservas.empresa', 'empresa')
      .select([
        'stands.id',
        'stands.codigo',
        'stands.area',
        'stands.precio',
        'stands.ubicacionX',
        'stands.ubicacionY',
        'stands.color',
        'stands.idPlanta',
        'stands.estado',
        'plantas.id',
        'plantas.nivel',
        'plantas.rutaModelo',
        'reservas.id',
        'reservas.monto',
        'reservas.fechaCreacion',
        'empresa.id',
        'empresa.nombreEmpresa',
      ])
      .where('stands.idPlanta = :idPlanta', { idPlanta })
      .getMany()
  }

  async listarReservas(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro, orden, sentido } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Reservas)
      .createQueryBuilder('reservas')
      .leftJoinAndSelect('reservas.empresa', 'empresa')
      .leftJoinAndSelect('reservas.stand', 'stand')
      .leftJoinAndSelect('stand.plantas', 'planta')
      .leftJoinAndSelect('planta.bloques', 'bloque')
      .select([
        'reservas.id',
        'reservas.monto',
        'reservas.fechaCreacion',
        'reservas.estado',
        'empresa.id',
        'empresa.nombreEmpresa',
        'stand.id',
        'stand.codigo',
        'stand.area',
        'stand.precio',
        'stand.ubicacionX',
        'stand.ubicacionY',
        'stand.color',
        'planta.id',
        'planta.nivel',
        'planta.rutaModelo',
        'bloque.id',
        'bloque.nombre',
      ])
      .take(limite)
      .skip(saltar)

    switch (orden) {
      case 'monto':
        query.addOrderBy('reservas.monto', sentido)
        break
      case 'fechaCreacion':
        query.addOrderBy('reservas.fechaCreacion', sentido)
        break
      default:
        query.orderBy('reservas.id', 'ASC')
    }

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('empresa.nombreEmpresa like :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('stand.codigo ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }
  async listarPorBloques(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro, orden, sentido } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Bloques)
      .createQueryBuilder('bloques')
      .select([
        'bloques.id',
        'bloques.nombre',
        'bloques.descripcion',
        'bloques.imagen',
      ])
      .take(limite)
      .skip(saltar)

    switch (orden) {
      case 'descripcion':
        query.addOrderBy('bloques.descripcion', sentido)
        break
      case 'nombre':
        query.addOrderBy('bloques.nombre', sentido)
        break
      default:
        query.orderBy('bloques.id', 'ASC')
    }

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('bloques.descripcion like :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('stands.nombre ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }

  async crear(standDto: CrearStandDto, usuarioAuditoria: string) {
    const { precio, area, codigo, ubicacionX, ubicacionY, color, idPlanta } =
      standDto

    const stand = new Stands()
    stand.precio = precio
    stand.area = area
    stand.codigo = codigo
    stand.color = color
    stand.idPlanta = idPlanta
    stand.ubicacionX = ubicacionX
    stand.ubicacionY = ubicacionY
    stand.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Stands).save(stand)
  }

  async actualizar(idStand: string, standDto: any, usuarioAuditoria: string) {
    const datosActualizar = new Stands({
      ...standDto,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Stands)
      .update(idStand, datosActualizar)
  }

  async reservar(standDto: ReservasDto, usuarioAuditoria: string) {
    const { monto, idEmpresa, idStand, idIngreso } = standDto
    const reserva = new Reservas()
    reserva.monto = monto
    reserva.idEmpresa = idEmpresa
    reserva.idStand = idStand
    reserva.idIngreso = idIngreso ?? ''
    reserva.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Reservas).save(reserva)
  }

  async eliminarReserva(idReserva: string) {
    //eliminamos de la tabla reservas la reserva
    return await this.dataSource
      .getRepository(Reservas)
      .createQueryBuilder('reservas')
      .delete()
      .from(Reservas)
      .where('id = :idReserva', { idReserva })
      .execute()
  }

  async eliminarStand(idStand: string) {
    return await this.dataSource
      .getRepository(Stands)
      .createQueryBuilder('stands')
      .delete()
      .from(Stands)
      .where('id = :idStand', { idStand })
      .execute()
  }

  async listarStands(idStand: string) {
    return await this.dataSource
      .getRepository(Stands)
      .createQueryBuilder('stands')
      .where('id = :idStand', { idStand })
      .getOne()
  }

  async listarIgreso(id: string) {
    return await this.dataSource
      .getRepository(Ingresos)
      .createQueryBuilder('ingresos')
      .where({ id: id })
      .getOne()
  }

  async listarReservaIngreso(id: string) {
    return await this.dataSource
      .getRepository(Reservas)
      .createQueryBuilder('reservas')
      .where({ id: id })
      .getOne()
  }

  async eliminarIngresoReserva(idIngreso: string) {
    return await this.dataSource
      .getRepository(Ingresos)
      .createQueryBuilder('ingresos')
      .delete()
      .from(Ingresos)
      .where('id = :idIngreso', { idIngreso })
      .execute()
  }
}
