import { Injectable } from '@nestjs/common'
import { BaseService } from 'src/common/base/base-service'
import { MailerService } from '@nestjs-modules/mailer'

@Injectable()
export class EmailService extends BaseService {
  constructor(private readonly mailerService: MailerService) {
    super()
  }

  async enviarDocumento(datosCorreo: any, file: any): Promise<void> {
    try {
      const htmlCorreo = `
  <!DOCTYPE html>
  <html>
  <head>
    <style>
      body {
        font-family: Arial, sans-serif;
      } 
      .container {
        background-color: #f2f2f2;
        padding: 20px;
        border-radius: 5px;
      }
      .header {
        text-align: center;
        font-size: 24px;
        font-weight: bold;
      }
      .highlight {
        color: #0078d4; /* Color azul destacado */
        font-weight: bold;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="header">Le enviamos el correo de invitacion a la feria</div>
      <p>Hola <span class="highlight">${datosCorreo.nombres}</span>,</p>
      <p>Le enviamos el documento de invitacion a la feria</p>
      <p>Adjuntamos el documento</p>

      <a href="http://localhost:3000/uploads/${file}">Descargar documento</a>
    </div>
  </body>
  </html>
`
      const mensaje = this.mailerService.sendMail({
        to: `${datosCorreo.correo}`,
        from: process.env.MAIL_USER,
        subject: `${datosCorreo.asunto}`,
        text: 'Correo de invitación a la feria',
        html: htmlCorreo,
        attachments: [
          {
            filename: file,
            path: `./uploads/${file}`,
          },
        ],
      })
      return mensaje
    } catch (e) {
      console.log('error al enviar correo', e)
    }
  }
  async enviarContrato(datosCorreo: any, file: string): Promise<void> {
    try {
      const htmlCorreo = `
  <!DOCTYPE html>
  <html>
  <head>
    <style>
      body {
        font-family: Arial, sans-serif;
      } 
      .container {
        background-color: #f2f2f2;
        padding: 20px;
        border-radius: 5px;
      }
      .header {
        text-align: center;
        font-size: 24px;
        font-weight: bold;
      }
      .highlight {
        color: #0078d4; /* Color azul destacado */
        font-weight: bold;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="header">Le enviamos el correo de contratto a feria</div>
      <p>Hola <span class="highlight">${datosCorreo.nombres}</span>,</p>
      <p>Le enviamos el documento cintrato feria</p>
      <p>Adjuntamos el documento</p>

    </div>
  </body>
  </html>
`
      const mensaje = this.mailerService.sendMail({
        to: `${datosCorreo.correo}`,
        from: process.env.MAIL_USER,
        subject: `${datosCorreo.asunto}`,
        text: 'Correo de invitación a la feria',
        html: htmlCorreo,
        attachments: [
          {
            path: file, // Ruta del archivo adjunto (PDF)
          },
        ],
      })
      return mensaje
    } catch (e) {
      console.log('error al enviar correo', e)
    }
  }
  async enviarEMail(password: string, datosCorreo: any): Promise<void> {
    try {
      const htmlCorreo = `
  <!DOCTYPE html>
  <html>
  <head>
    <style>
      body {
        font-family: Arial, sans-serif;
      }
      .container {
        background-color: #f2f2f2;
        padding: 20px;
        border-radius: 5px;
      }
      .header {
        text-align: center;
        font-size: 24px;
        font-weight: bold;
      }
      .highlight {
        color: #0078d4; /* Color azul destacado */
        font-weight: bold;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="header">Bienvenido al sistema</div>
      <p>Hola <span class="highlight">${datosCorreo.nombres}</span>,</p>
      <p>Tu usuario es: <span class="highlight">${datosCorreo.usuario}</span></p>
      <p>Tu contraseña temporal es: <span class="highlight">${password}</span></p>
      <p>No olvides cambiar tu contraseña.</p>
    </div>
  </body>
  </html>
`
      const mensaje = this.mailerService.sendMail({
        to: `${datosCorreo.correo}`,
        from: process.env.MAIL_USER,
        subject: `${datosCorreo.asunto}`,
        text: 'Bienvenida',
        html: htmlCorreo,
      })
      return mensaje
    } catch (e) {
      console.log('error al enviar correo', e)
    }
  }
}
