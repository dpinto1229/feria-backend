import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { ParamIdDto } from '../../../common/dto/params-id.dto'
import { Request } from 'express'
import { DocumentoService } from '../service/documento.service'
import { FileInterceptor } from '@nestjs/platform-express'
import { diskStorage } from 'multer'
import { extname } from 'path'
import path from 'path'
import { Response } from 'express'
@Controller('documento')
export class DocumentoController extends BaseController {
  constructor(private empresaService: DocumentoService) {
    super()
  }

  @UseGuards(JwtAuthGuard, CasbinGuard)
  @Get()
  async listar(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.empresaService.listar(paginacionQueryDto)
    return this.successList(result)
  }

  @UseGuards(JwtAuthGuard, CasbinGuard)
  @Get('/contratos')
  async listarContratos(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.empresaService.listarContratos(paginacionQueryDto)
    return this.successList(result)
  }
  @UseGuards(JwtAuthGuard, CasbinGuard)
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('')
          return cb(null, `${randomName}${extname(file.originalname)}`)
        },
      }),
    })
  )
  async crear(
    @Req() req: Request,
    @Body() parametroDto: any,
    @UploadedFile() file: Express.Multer.File
  ) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.crear(
      parametroDto,
      usuarioAuditoria,
      file
    )
    return this.successCreate(result)
  }
  @UseGuards(JwtAuthGuard, CasbinGuard)
  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() parametroDto: any
  ) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.actualizarDatos(
      idParametro,
      parametroDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
  @UseGuards(JwtAuthGuard, CasbinGuard)
  @Patch('/:id/activacion')
  async activar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.activar(
      idParametro,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
  @UseGuards(JwtAuthGuard, CasbinGuard)
  @Patch('/:id/inactivacion')
  async inactivar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.inactivar(
      idParametro,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
  @Get('/:nombre')
  obtenerFotoPerfil(@Param() param: { nombre: string }, @Res() res: Response) {
    const { nombre } = param
    return res.sendFile(decodeURIComponent(nombre), {
      root: path.resolve(String('./uploads')),
    })
  }

  @Get('/contrato/:nombre')
  obtenerContrato(@Param() param: { nombre: string }, @Res() res: Response) {
    const { nombre } = param
    return res.sendFile(nombre, {
      root: path.resolve(String('./contratos')),
    })
  }
}
