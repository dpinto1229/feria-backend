import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class CrearIngresoDto {
  descripcion: string

  monto: number

  tipo: string

  idUsuario: string

  idIngreso?: string
}

export class ActualizarIngresoDto {
  descripcion: string

  monto: number

  tipo: string

  estado: string
}
