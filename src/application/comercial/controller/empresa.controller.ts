import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { ParamIdDto } from '../../../common/dto/params-id.dto'
import { Request } from 'express'
import { EmpresaService } from '../service/empresa.service'
import { CrearActualizarEmpresaDto } from '../dto/empresaDto'

@Controller('empresas')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class EmpresaController extends BaseController {
  constructor(private empresaService: EmpresaService) {
    super()
  }

  @Get()
  async listar(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.empresaService.listar(paginacionQueryDto)
    return this.successListRows(result)
  }

  @Post()
  async crear(
    @Req() req: Request,
    @Body() parametroDto: CrearActualizarEmpresaDto
  ) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.crear(
      parametroDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }
  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() parametroDto: CrearActualizarEmpresaDto
  ) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.actualizarDatos(
      idParametro,
      parametroDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/:id/activacion')
  async activar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.activar(
      idParametro,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/:id/inactivacion')
  async inactivar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.empresaService.inactivar(
      idParametro,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
}
