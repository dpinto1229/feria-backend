import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Documento } from '../entities/invitacion.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Empresas } from '../entities/empresas.entity'

@Injectable()
export class DocumentoRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Documento)
      .createQueryBuilder('documento')
      .where({ id: id })
      .getOne()
  }

  async busacarPorEmpresa(id: string) {
    return await this.dataSource
      .getRepository(Empresas)
      .createQueryBuilder('empresa')
      .where({ id: id })
      .getOne()
  }

  async actualizar(id: string, documentoDto: any, usuarioAuditoria: string) {
    const datosActualizar = new Documento({
      ...documentoDto,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Documento)
      .update(id, datosActualizar)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Documento)
      .createQueryBuilder('documento')
      .leftJoinAndSelect('documento.empresa', 'empresa')
      .select([
        'documento.id',
        'documento.documentoInvitacion',
        'documento.tipoDocumento',
        'documento.descripcion',
        'documento.estado',
        'empresa.id',
        'empresa.nombreEmpresa',
        'empresa.descripcionEmpresa',
        'empresa.rubroEmpresa',
        'empresa.telefonoEmpresa',
      ])
      .orderBy('documento.id', 'ASC')
      .take(limite)
      .skip(saltar)
      .where('documento.tipoDocumento = :tipoDocumento', {
        tipoDocumento: 'INVITACION',
      })

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('Documento.nombreEmpresa like :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('Documento.descripcionEmpresa ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('Documento.rubroEmpresa ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('Documento.telefonoEmpresa ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }

  async listarContratos(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Documento)
      .createQueryBuilder('documento')
      .leftJoinAndSelect('documento.empresa', 'empresa')
      .select([
        'documento.id',
        'documento.documentoInvitacion',
        'documento.tipoDocumento',
        'documento.descripcion',
        'documento.estado',
        'empresa.id',
        'empresa.nombreEmpresa',
        'empresa.descripcionEmpresa',
        'empresa.rubroEmpresa',
        'empresa.telefonoEmpresa',
      ])
      .orderBy('documento.id', 'ASC')
      .take(limite)
      .skip(saltar)
      .where('documento.tipoDocumento = :tipoDocumento', {
        tipoDocumento: 'CONTRATO',
      })

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('Documento.nombreEmpresa like :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('Documento.descripcionEmpresa ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('Documento.rubroEmpresa ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('Documento.telefonoEmpresa ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }

  async crear(
    empresaDto: any,
    usuarioAuditoria: string,
    file: Express.Multer.File
  ) {
    const { idEmpresa, descripcion, tipoDocumento } = empresaDto

    const documento = new Documento()
    documento.documentoInvitacion = file.filename
    documento.tipoDocumento = tipoDocumento
    documento.descripcion = descripcion
    documento.idEmpresa = idEmpresa
    documento.estado = ParametroEstado.ENVIADO
    documento.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Documento).save(documento)
  }

  async crearDocumento(documentoDto: any, usuarioAuditoria: string) {
    const { idEmpresa, documentoInvitacion, tipoDocumento, descripcion } =
      documentoDto
    const documento = new Documento()
    documento.documentoInvitacion = documentoInvitacion
    documento.idEmpresa = idEmpresa
    documento.tipoDocumento = tipoDocumento
    documento.descripcion = descripcion
    documento.estado = ParametroEstado.ENVIADO
    documento.usuarioCreacion = usuarioAuditoria
    return await this.dataSource.getRepository(Documento).save(documento)
  }
}
