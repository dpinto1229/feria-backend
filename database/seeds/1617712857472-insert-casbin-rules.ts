import { CasbinRule } from '../../src/core/authorization/entity/casbin.entity'
import { RolEnum } from '../../src/core/authorization/rol.enum'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class insertCasbinRules1617712857472 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const frontendRoutes: CasbinValue = {
      '/admin/usuarios': {
        [RolEnum.ADMINISTRADOR]: 'read|update|create|delete',
      },
      '/admin/perfil': {
        [RolEnum.ADMINISTRADOR]: 'read|update',
        [RolEnum.TODOS]: 'read|update',
        [RolEnum.COMERCIAL]: 'read|update',
        [RolEnum.COMUNICACION]: 'read|update',
        [RolEnum.DISEÑO]: 'read|update',
        [RolEnum.LEGAL]: 'read|update',
        [RolEnum.SISTEMAS]: 'read|update',
      },

      '/admin/home': {
        [RolEnum.ADMINISTRADOR]: 'read',
        [RolEnum.TODOS]: 'read',
        [RolEnum.COMERCIAL]: 'read',
        [RolEnum.COMUNICACION]: 'read',
        [RolEnum.DISEÑO]: 'read',
        [RolEnum.LEGAL]: 'read',
        [RolEnum.SISTEMAS]: 'read',
      },
      '/admin/roles': {
        [RolEnum.ADMINISTRADOR]: 'read|create|update|delete',
        [RolEnum.SISTEMAS]: 'read|create|update|delete',
      },
      '/admin/parametros': {
        [RolEnum.ADMINISTRADOR]: 'read|update|create',
        [RolEnum.SISTEMAS]: 'read|create|update|delete',
      },
      '/admin/modulos': {
        [RolEnum.ADMINISTRADOR]: 'read|update|create',
        [RolEnum.SISTEMAS]: 'read|create|update|delete',
      },

      '/admin/politicas': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.SISTEMAS]: 'read|create|update|delete',
      },
      '/admin/empresas': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.COMERCIAL]: 'create|read|update|delete',
        [RolEnum.LEGAL]: 'create|read|update|delete',
      },
      '/admin/invitacion': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.LEGAL]: 'create|read|update|delete',
      },
      '/admin/stands': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.COMERCIAL]: 'create|read|update|delete',
      },
      '/admin/seguimiento': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.COMERCIAL]: 'create|read|update|delete',
        [RolEnum.SISTEMAS]: 'create|read|update|delete',
      },
      '/admin/reportes': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
      },
      '/admin/dashboard': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
      },
      '/admin/reservas': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.COMERCIAL]: 'create|read|update|delete',
      },
      '/admin/ingresos': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
      },
      '/admin/contratos': {
        [RolEnum.ADMINISTRADOR]: 'create|read|update|delete',
        [RolEnum.LEGAL]: 'create|read|update|delete',
      },
    }

    const backendRoutes: CasbinValue = {
      '/api/autorizacion/politicas': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST|DELETE|PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },
      '/api/autorizacion/modulos': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST|DELETE|PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/modulos/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/modulos/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST|DELETE|PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/modulos/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST|DELETE|PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/roles': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/roles/todos': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },
      '/api/autorizacion/roles/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/roles/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/roles/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/usuarios': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/usuarios/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/usuarios/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/usuarios/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },
      '/api/usuarios/:id/restauracion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/usuarios/:id/reenviar': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/autorizacion/permisos': {
        [RolEnum.TODOS]: 'GET',
      },

      '/api/usuarios/cuenta/perfil': {
        [RolEnum.TODOS]: 'GET',
      },

      '/api/usuarios/cuenta/contrasena': {
        [RolEnum.TODOS]: 'PATCH',
      },
      '/api/parametros': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },
      '/api/parametros/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/parametros/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/parametros/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.TODOS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/parametros/:grupo/listado': {
        [RolEnum.TODOS]: 'GET',
      },
      '/api/empresas': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },
      '/api/empresas/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },

      '/api/empresas/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },

      '/api/empresas/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },
      '/api/documento': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.SISTEMAS]: 'GET|POST|DELETE|PATCH',
      },
      '/api/documento/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.SISTEMAS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/documento/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.SISTEMAS]: 'GET|POST|DELETE|PATCH',
      },

      '/api/documento/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.SISTEMAS]: 'GET|POST|DELETE|PATCH',
      },
      '/api/seguimiento': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST|DELETE|PATCH',
        [RolEnum.SISTEMAS]: 'GET|POST|DELETE|PATCH',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },
      '/api/seguimiento/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.SISTEMAS]: 'GET|POST|DELETE|PATCH',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },
      '/api/usuarios/perfil/:id': {
        [RolEnum.TODOS]: 'GET|PATCH',
      },
      '/api/dashboard/cards': {
        [RolEnum.ADMINISTRADOR]: 'GET',
      },
      '/api/dashboard/charts': {
        [RolEnum.ADMINISTRADOR]: 'GET',
      },
      '/api/stands/bloques': {
        [RolEnum.ADMINISTRADOR]: 'GET',
        [RolEnum.COMERCIAL]: 'GET',
      },
      '/api/stands': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },
      '/api/stands/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.COMERCIAL]: 'GET|POST|DELETE|PATCH',
      },
      '/api/stands/bloques/:id': {
        [RolEnum.ADMINISTRADOR]: 'GET',
        [RolEnum.COMERCIAL]: 'GET',
      },
      '/api/stands/plantas/:id': {
        [RolEnum.ADMINISTRADOR]: 'GET',
        [RolEnum.COMERCIAL]: 'GET',
      },
      '/api/stands/reservar': {
        [RolEnum.ADMINISTRADOR]: 'POST',
        [RolEnum.COMERCIAL]: 'POST',
      },
      '/api/stands/reservas': {
        [RolEnum.ADMINISTRADOR]: 'GET',
        [RolEnum.COMERCIAL]: 'GET',
      },
      '/api/stands/eliminar/reserva/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.COMERCIAL]: 'PATCH',
      },
      '/api/usuarios/validar-contrasena': {
        [RolEnum.TODOS]: 'POST',
      },
      '/api/ingresos': {
        [RolEnum.ADMINISTRADOR]: 'GET|POST',
      },
      '/api/ingresos/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
      },

      '/api/ingresos/:id/activacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
      },

      '/api/ingresos/:id/inactivacion': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
      },
      '/api/ingresos/presupuesto': {
        [RolEnum.ADMINISTRADOR]: 'GET',
      },
      '/api/stands/contrato': {
        [RolEnum.ADMINISTRADOR]: 'POST',
        [RolEnum.COMERCIAL]: 'POST',
      },
      '/api/documento/contratos': {
        [RolEnum.ADMINISTRADOR]: 'POST|GET',
        [RolEnum.LEGAL]: 'POST|GET',
      },

      '/api/stands/eliminar/:id': {
        [RolEnum.ADMINISTRADOR]: 'PATCH',
        [RolEnum.COMERCIAL]: 'PATCH',
      },
    }

    const registrarCasbin = async (
      valoresCasbin: CasbinValue,
      tipo: string
    ) => {
      for (const routePath of Object.keys(valoresCasbin)) {
        const rolNameList = Object.keys(valoresCasbin[routePath])
        for (const rolName of rolNameList) {
          const action = valoresCasbin[routePath][rolName]
          const datosRegistro = new CasbinRule({
            ptype: 'p',
            v0: rolName,
            v1: routePath,
            v2: action,
            v3: tipo,
          })
          await queryRunner.manager.save(datosRegistro)
        }
      }
    }

    await registrarCasbin(frontendRoutes, 'frontend')
    await registrarCasbin(backendRoutes, 'backend')
  }

  /* eslint-disable */
  public async down(queryRunner: QueryRunner): Promise<void> {}
}

export type RouteItem = {
  [key: string]: string
}

export type CasbinValue = {
  [key: string]: RouteItem
}
