import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { Empresas } from '../entities/empresas.entity'
import { Usuario } from 'src/core/usuario/entity/usuario.entity'
import { Documento } from '../entities/invitacion.entity'
import { Seguimiento } from '../entities/seguimiento.entity'

@Injectable()
export class DashboardRepository {
  constructor(private dataSource: DataSource) {}

  async listarTotalEmpresas() {
    return await this.dataSource
      .getRepository(Empresas)
      .createQueryBuilder('empresa')
      .getCount()
  }

  async listarTotalUsuarios() {
    return await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('Usuario')
      .getCount()
  }

  async listarTotalDocumentos() {
    return await this.dataSource
      .getRepository(Seguimiento)
      .createQueryBuilder('seguimiento')
      .getCount()
  }

  async listarDocumentosPorEmpresa() {
    return await this.dataSource
      .getRepository(Seguimiento)
      .createQueryBuilder('documento')
      .leftJoinAndSelect('documento.empresa', 'empresa')
      .select([
        'documento.id',
        'documento.estado',
        'empresa.id',
        'empresa.nombreEmpresa',
        'empresa.descripcionEmpresa',
        'empresa.rubroEmpresa',
        'empresa.telefonoEmpresa',
        'empresa.evento',
      ])
      .orderBy('documento.id', 'ASC')
      .getMany()
  }
}
