import { MigrationInterface, QueryRunner } from 'typeorm'
import { USUARIO_SISTEMA } from '../../src/common/constants'
import { Stands } from 'src/application/comercial/entities/stands.entity'

export class insertsStands1617820337612 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const items = [
      {
        codigo: 'STAND-88',
        area: 9,
        precio: 1000,
        color: '#00FF00',
        ubicacionX: 0,
        ubicacionY: 0,
        idPlanta: '1',
      },
      {
        codigo: 'STAND-89',
        area: 9,
        precio: 1000,
        color: '#00FF00',
        ubicacionX: 0,
        ubicacionY: 20,
        idPlanta: '1',
      },
      {
        codigo: 'STAND-90',
        area: 9,
        precio: 1000,
        color: '#00FF00',
        ubicacionX: 0,
        ubicacionY: 40,
        idPlanta: '1',
      },
      {
        codigo: 'STAND-91',
        area: 9,
        precio: 1000,
        color: '#00FF00',
        ubicacionX: 0,
        ubicacionY: 60,
        idPlanta: '1',
      },
      {
        codigo: 'STAND-92',
        area: 9,
        precio: 1000,
        color: '#00FF00',
        ubicacionX: 0,
        ubicacionY: 80,
        idPlanta: '1',
      },
      {
        codigo: 'STAND-93',
        area: 9,
        precio: 1000,
        color: '#00FF00',
        ubicacionX: 0,
        ubicacionY: 100,
        idPlanta: '1',
      },
    ]
    const stands = items.map((item) => {
      return new Stands({
        codigo: item.codigo,
        area: item.area,
        precio: item.precio,
        color: item.color,
        ubicacionX: item.ubicacionX,
        ubicacionY: item.ubicacionY,
        idPlanta: item.idPlanta,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(stands)
  }

  /* eslint-disable */
  public async down(queryRunner: QueryRunner): Promise<void> {}
} 