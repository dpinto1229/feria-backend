import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { UsuarioRol } from '../../authorization/entity/usuario-rol.entity'
import { Persona } from './persona.entity'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { Status } from '../../../common/constants'
import { Ingresos } from 'src/application/comercial/entities/ingresos.entity'

dotenv.config()

export const UsuarioEstado = {
  ACTIVE: Status.ACTIVE,
  INACTIVE: Status.INACTIVE,
  CREATE: Status.CREATE,
  PENDING: Status.PENDING,
}

@Check(UtilService.buildStatusCheck(UsuarioEstado))
@Entity({ name: 'usuarios', schema: process.env.DB_SCHEMA_USUARIOS })
export class Usuario extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Usuario',
  })
  id: string

  @Column({
    length: 50,
    type: 'varchar',
    unique: true,
    comment: 'nombre de usuario, usualmente carnet de identidad',
  })
  usuario: string

  @Column({
    length: 255,
    type: 'varchar',
    comment: 'contraseña del usuario',
  })
  contrasena: string

  @Column({
    name: 'avatar',
    length: 100,
    type: 'varchar',
    nullable: true,
    comment: 'avatar del usuario',
  })
  avatar: string

  @Column({
    name: 'correo_electronico',
    type: 'varchar',
    nullable: true,
    comment: 'correo electrónico del usuario',
  })
  correoElectronico?: string | null

  @Column({
    type: 'integer',
    default: 0,
    comment: 'número de intentos de inicio de sesión fallidos',
  })
  intentos: number

  @Column({
    name: 'id_persona',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla de Personas',
  })
  idPersona: string

  @OneToMany(() => UsuarioRol, (usuarioRol) => usuarioRol.usuario)
  usuarioRol: UsuarioRol[]

  @OneToMany(() => Ingresos, (ingreso) => ingreso.usuario)
  ingreso: Ingresos[]

  @ManyToOne(() => Persona, (persona) => persona.usuarios, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_persona',
    referencedColumnName: 'id',
  })
  persona: Persona

  constructor(data?: Partial<Usuario>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || UsuarioEstado.ACTIVE
  }
}
