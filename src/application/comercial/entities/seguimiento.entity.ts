import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Empresas } from './empresas.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(ParametroEstado))
@Entity({ name: 'seguimiento_empresas', schema: process.env.DB_SCHEMA_FERIA })
export class Seguimiento extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 150,
    type: 'varchar',
    name: 'tipo',
    comment: 'Tipo de seguimiento',
  })
  tipo: string

  @Column({
    length: 150,
    type: 'varchar',
    name: 'medio',
    comment: 'medio de seguimiento',
  })
  medio: string

  @Column({
    length: 150,
    type: 'varchar',
    name: 'archivado',
    comment: 'archivado',
  })
  archivado: string

  @Column({
    length: 300,
    type: 'varchar',
    name: 'observaciones',
    comment: 'observaciones',
  })
  observaciones: string

  @Column({
    length: 300,
    type: 'varchar',
    name: 'estado_empresa',
    nullable: true,
    comment: 'estado de la empresa',
  })
  estadoEmpresa: string

  @Column({
    name: 'id_empresa',
    type: 'bigint',
    nullable: true,
    comment: 'clave foránea que referencia la tabla de Personas',
  })
  idEmpresa: string

  @ManyToOne(() => Empresas, (empresa) => empresa.seguimiento, {
    nullable: true,
  })
  @JoinColumn({
    name: 'id_empresa',
    referencedColumnName: 'id',
  })
  empresa: Empresas

  constructor(data?: Partial<Seguimiento>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ParametroEstado.ACTIVO
  }
}
