import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Bloques } from './bloques.entity'
import { Stands } from './stands.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(ParametroEstado))
@Entity({ name: 'plantas', schema: process.env.DB_SCHEMA_FERIA })
export class Plantas extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({ length: 50, type: 'varchar', comment: 'Nombre de parámetro' })
  nivel: string
  @Column({
    length: 255,
    type: 'varchar',
    comment: 'Descripción de parámetro',
    nullable: true,
  })
  descripcion: string
  @Column({
    length: 255,
    type: 'varchar',
    nullable: true,
    comment: 'Imagen de parámetro',
  })
  imagen: string
  @Column({
    name: 'id_bloque',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla de Personas',
  })
  idBloque: string

  @Column({
    length: 255,
    type: 'varchar',
    nullable: true,
    comment: 'link de la imagen del stand',
  })
  rutaModelo: string
  @ManyToOne(() => Bloques, (bloques) => bloques.plantas, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_bloque',
    referencedColumnName: 'id',
  })
  bloques: Bloques

  @OneToMany(() => Stands, (stands) => stands.plantas)
  stands: Stands[]

  constructor(data?: Partial<Plantas>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ParametroEstado.ACTIVO
  }
}
