import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Plantas } from './plantas.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(ParametroEstado))
@Entity({ name: 'bloques', schema: process.env.DB_SCHEMA_FERIA })
export class Bloques extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 255,
    type: 'varchar',
    nullable: false,
    comment: 'Nombre de parámetro',
  })
  nombre: string
  @Column({
    length: 255,
    type: 'varchar',
    nullable: true,
    comment: 'Descripción de parámetro',
  })
  descripcion: string

  @Column({
    length: 255,
    type: 'varchar',
    nullable: true,
    comment: 'Imagen de parámetro',
  })
  imagen: string

  @OneToMany(() => Plantas, (plantas) => plantas.bloques)
  plantas: Plantas[]

  constructor(data?: Partial<Bloques>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ParametroEstado.ACTIVO
  }
}
