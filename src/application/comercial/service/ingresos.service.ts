import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Messages } from '../../../common/constants/response-messages'
import { IngresosRepository } from '../repository/ingresos.respositoriy'
import { ActualizarIngresoDto, CrearIngresoDto } from '../dto/ingresosDto'
import { ParametroEstado } from 'src/application/parametro/constant'

@Injectable()
export class IngresosService extends BaseService {
  constructor(
    @Inject(IngresosRepository)
    private ingresoRepositorio: IngresosRepository
  ) {
    super()
  }

  async crear(parametroDto: CrearIngresoDto, usuarioAuditoria: string) {
    return await this.ingresoRepositorio.crear(parametroDto, usuarioAuditoria)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.ingresoRepositorio.listar(paginacionQueryDto)
  }

  async actualizarDatos(
    id: string,
    parametroDto: ActualizarIngresoDto,
    usuarioAuditoria: string
  ) {
    const parametro = await this.ingresoRepositorio.buscarPorId(id)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    await this.ingresoRepositorio.actualizar(id, parametroDto, usuarioAuditoria)
    return { id }
  }

  async activar(idParametro: string, usuarioAuditoria: string) {
    const parametro = await this.ingresoRepositorio.buscarPorId(idParametro)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const parametroDto = new ActualizarIngresoDto()
    parametroDto.estado = ParametroEstado.ACTIVO
    await this.ingresoRepositorio.actualizar(
      idParametro,
      parametroDto,
      usuarioAuditoria
    )
    return {
      id: idParametro,
      estado: parametroDto.estado,
    }
  }

  async inactivar(idParametro: string, usuarioAuditoria: string) {
    const parametro = await this.ingresoRepositorio.buscarPorId(idParametro)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const parametroDto = new ActualizarIngresoDto()
    parametroDto.estado = ParametroEstado.INACTIVO
    await this.ingresoRepositorio.actualizar(
      idParametro,
      parametroDto,
      usuarioAuditoria
    )
    return {
      id: idParametro,
      estado: parametroDto.estado,
    }
  }

  async listarPresupuesto() {
    const ingreso = await this.ingresoRepositorio.listarPresupuesto()
    //sumamos el monto de los presupuestos
    let total = 0
    ingreso.forEach((element) => {
      total += Number(element.monto)
    })

    const egreso = await this.ingresoRepositorio.listarEgresos()
    //restamos el monto de egresos si es que hay
    if (egreso.length > 0) {
      egreso.forEach((element) => {
        total -= Number(element.monto)
      })
    }
    const respuesta = {
      total,
    }
    return respuesta
  }
}
