import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { Request } from 'express'
import { StandService } from '../service/stands.service'
import { CrearStandDto } from '../repository/stands.repository'
import { ParamIdDto } from 'src/common/dto/params-id.dto'
import { ReservasDto } from '../dto/standsDto'

@Controller('stands')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class StandsController extends BaseController {
  constructor(private standService: StandService) {
    super()
  }

  @Get('/bloques')
  async listarPorBloques(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.standService.listarPorBloques(paginacionQueryDto)
    return this.successListRows(result)
  }

  @Get('/reservas')
  async listarReservas(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.standService.listarReservas(paginacionQueryDto)
    return this.successListRows(result)
  }
  @Get('/bloques/:id')
  async listarDetalleBloques(@Param() params: ParamIdDto) {
    const { id: idBloque } = params
    const result = await this.standService.listarDetalle(idBloque)
    return this.successList(result)
  }
  @Get('/plantas/:id')
  async listarStandsPorPlantas(@Param() params: ParamIdDto) {
    const { id: idPlanta } = params
    const result = await this.standService.listarPorPlanta(idPlanta)
    return this.successList(result)
  }
  @Post()
  async crear(@Req() req: Request, @Body() standDto: CrearStandDto) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.standService.crear(standDto, usuarioAuditoria)
    return this.successCreate(result)
  }
  @Post('/reservar')
  async reservar(@Req() req: Request, @Body() standDto: ReservasDto) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.standService.reservar(standDto, usuarioAuditoria)
    return this.successCreate(result)
  }

  @Post('/contrato')
  async generarContrato(@Req() req: Request, @Body() standDto: ReservasDto) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.standService.generarContrato(
      standDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }
  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() standDto: CrearStandDto
  ) {
    const { id: idStand } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.standService.actualizar(
      idStand,
      standDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/eliminar/reserva/:id')
  async eliminarReserva(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() standDto: any
  ) {
    const { id: idReserva } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.standService.eliminarReserva(
      idReserva,
      standDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/eliminar/:id')
  async eliminar(@Param() params: ParamIdDto, @Req() req: Request) {
    const { id: idStand } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.standService.eliminarStand(idStand)
    return this.successUpdate(result)
  }
}
