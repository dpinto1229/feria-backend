import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { Stands } from './stands.entity'
import { Empresas } from './empresas.entity'
import { UtilService } from 'src/common/lib/util.service'
import { Ingresos } from './ingresos.entity'

export enum ReservaEstado {
  RESERVADO = 'RESERVADO',
  CANCELADO = 'CANCELADO',
}
@Check(UtilService.buildStatusCheck(ReservaEstado))
@Entity({ name: 'reservas', schema: process.env.DB_SCHEMA_FERIA })
export class Reservas extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Reservas',
  })
  id: string

  @Column({
    type: 'decimal',
    precision: 10,
    scale: 2,
    nullable: false,
    comment: 'Monto de la reserva',
  })
  monto: number

  @Column({
    name: 'id_empresa',
    type: 'bigint',
    nullable: false,
    comment: 'Clave foránea de la tabla Empresas',
  })
  idEmpresa: string

  @ManyToOne(() => Empresas, (empresa) => empresa.reservas)
  @JoinColumn({ name: 'id_empresa', referencedColumnName: 'id' })
  empresa: Empresas

  @Column({
    name: 'id_stand',
    type: 'bigint',
    nullable: false,
    comment: 'Clave foránea de la tabla Stands',
  })
  idStand: string

  @Column({
    name: 'id_ingreso',
    type: 'bigint',
    nullable: true,
    comment: 'Clave foránea de la tabla ingresos',
  })
  idIngreso: string

  @ManyToOne(() => Stands, (stand) => stand.reservas)
  @JoinColumn({ name: 'id_stand', referencedColumnName: 'id' })
  stand: Stands

  @ManyToOne(() => Ingresos, (ingresos) => ingresos.reservas)
  @JoinColumn({ name: 'id_ingreso', referencedColumnName: 'id' })
  ingreso: Ingresos
  constructor(data?: Partial<Reservas>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ReservaEstado.RESERVADO
  }
}
