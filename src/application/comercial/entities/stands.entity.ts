import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { Plantas } from './plantas.entity'
import { Reservas } from './reserva.entity'

dotenv.config()

export enum StandsEstado {
  ACTIVO = 'ACTIVO',
  INACTIVO = 'INACTIVO',
  DISPONIBLE = 'DISPONIBLE',
  RESERVADO = 'RESERVADO',
}
export const ColorStandsEstado = {
  [StandsEstado.ACTIVO]: '#00FF00',
  [StandsEstado.INACTIVO]: '#FF0000',
  [StandsEstado.DISPONIBLE]: '#0000FF',
  [StandsEstado.RESERVADO]: '#6c6c6c',
}
@Check(UtilService.buildStatusCheck(StandsEstado))
@Entity({ name: 'stands', schema: process.env.DB_SCHEMA_FERIA })
export class Stands extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Código o número identificador del stand',
  })
  codigo: string

  @Column({
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Área del stand en metros cuadrados',
  })
  area: number

  //precio
  @Column({
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Precio del stand',
  })
  precio: number

  //color
  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Color del stand',
  })
  color: string

  //ubicacion en el plano en el eje x
  @Column({
    name: 'ubicacion_x',
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Ubicación en el plano en el eje x',
  })
  ubicacionX: number

  //ubicacion en el plano en el eje y

  @Column({
    name: 'ubicacion_y',
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Ubicación en el plano en el eje y',
  })
  ubicacionY: number

  @Column({
    name: 'id_planta',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla de Personas',
  })
  idPlanta: string

  @ManyToOne(() => Plantas, (plantas) => plantas.stands, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_planta',
    referencedColumnName: 'id',
  })
  plantas: Plantas

  @OneToMany(() => Reservas, (reservas) => reservas.stand)
  reservas: Reservas[]

  constructor(data?: Partial<Stands>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || StandsEstado.ACTIVO
    this.color = this.color || ColorStandsEstado[this.estado]
  }
}
