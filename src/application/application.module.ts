import { Module } from '@nestjs/common'
import { ParametroModule } from './parametro/parametro.module'
import { ComercialModule } from './comercial/comercial.module'

@Module({
  imports: [ParametroModule, ComercialModule],
})
export class ApplicationModule {}
