import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { Request } from 'express'
import { SeguimientoService } from '../service/seguimiento.service'
import { ParamIdDto } from 'src/common/dto/params-id.dto'

@Controller('seguimiento')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class SeguimientoController extends BaseController {
  constructor(private seguimieintoService: SeguimientoService) {
    super()
  }

  @Get()
  async listar(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.seguimieintoService.listar(paginacionQueryDto)
    return this.successListRows(result)
  }

  @Post()
  async crear(@Req() req: Request, @Body() parametroDto: any) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.seguimieintoService.crear(
      parametroDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }

  @Patch(':id')
  async actualizar(
    @Req() req: Request,
    @Body() parametroDto: any,
    @Param() params: ParamIdDto
  ) {
    const usuarioAuditoria = this.getUser(req)
    const { id: idParametro } = params
    const result = await this.seguimieintoService.actualizar(
      idParametro,
      parametroDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
}
