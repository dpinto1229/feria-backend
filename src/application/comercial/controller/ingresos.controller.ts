import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { ParamIdDto } from '../../../common/dto/params-id.dto'
import { Request } from 'express'
import { IngresosService } from '../service/ingresos.service'
import { ActualizarIngresoDto, CrearIngresoDto } from '../dto/ingresosDto'
@Controller('ingresos')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class IngresoController extends BaseController {
  constructor(private ingresoService: IngresosService) {
    super()
  }

  @Get()
  async listar(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.ingresoService.listar(paginacionQueryDto)
    return this.successListRows(result)
  }

  @Post()
  async crear(@Req() req: Request, @Body() parametroDto: CrearIngresoDto) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.ingresoService.crear(
      parametroDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }
  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() parametroDto: ActualizarIngresoDto
  ) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.ingresoService.actualizarDatos(
      idParametro,
      parametroDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/:id/activacion')
  async activar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.ingresoService.activar(
      idParametro,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/:id/inactivacion')
  async inactivar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idParametro } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.ingresoService.inactivar(
      idParametro,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Get('/presupuesto')
  async listarPresupuesto() {
    const result = await this.ingresoService.listarPresupuesto()
    return this.successList(result)
  }
}
