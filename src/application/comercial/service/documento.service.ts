import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Messages } from '../../../common/constants/response-messages'
import { ParametroEstado } from 'src/application/parametro/constant'
import { DocumentoRepository } from '../repository/documento.repository'
import { EmailService } from 'src/application/mensajeria/service/mensajeria.service'

@Injectable()
export class DocumentoService extends BaseService {
  constructor(
    @Inject(DocumentoRepository)
    private documentoRepositorio: DocumentoRepository,
    private emailService: EmailService
  ) {
    super()
  }

  async crear(
    empresaDto: any,
    usuarioAuditoria: string,
    file: Express.Multer.File
  ) {
    const documento = await this.documentoRepositorio.crear(
      empresaDto,
      usuarioAuditoria,
      file
    )

    const empresas = await this.documentoRepositorio.busacarPorEmpresa(
      empresaDto.idEmpresa
    )
    const datosCorreo = {
      correo: empresas?.correoEmpresa,
      nombres: empresas?.nombreEncargado,
      asunto: Messages.DOCUMENT_INVITACION,
    }
    console.log('datosCorreo', datosCorreo)
    console.log('file', file)
    await this.emailService.enviarDocumento(datosCorreo, file.filename)
    return documento
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const respuesta = await this.documentoRepositorio.listar(paginacionQueryDto)

    const data = respuesta[0].map((item) => {
      return {
        ...item,
        documentoInvitacion: `${process.env.URL_BACKEND}documento/${item.documentoInvitacion}`,
      }
    })
    return {
      total: respuesta[1],
      filas: data,
    }
  }

  async listarContratos(paginacionQueryDto: PaginacionQueryDto) {
    const respuesta =
      await this.documentoRepositorio.listarContratos(paginacionQueryDto)

    const data = respuesta[0].map((item) => {
      return {
        ...item,
        documentoInvitacion: `${process.env.URL_BACKEND}documento/contrato/${item.documentoInvitacion}`,
      }
    })
    return {
      total: respuesta[1],
      filas: data,
    }
  }

  async actualizarDatos(id: string, empresaDto: any, usuarioAuditoria: string) {
    const empresa = await this.documentoRepositorio.buscarPorId(id)
    if (!empresa) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    await this.documentoRepositorio.actualizar(id, empresaDto, usuarioAuditoria)
    return { id }
  }

  async activar(idEmpresa: string, usuarioAuditoria: string) {
    const parametro = await this.documentoRepositorio.buscarPorId(idEmpresa)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    await this.documentoRepositorio.actualizar(
      idEmpresa,
      {
        estado: ParametroEstado.RECEPCIONADO,
      },
      usuarioAuditoria
    )
    return {
      id: idEmpresa,
      estado: ParametroEstado.RECEPCIONADO,
    }
  }

  async inactivar(idEmpresa: string, usuarioAuditoria: string) {
    const parametro = await this.documentoRepositorio.buscarPorId(idEmpresa)
    if (!parametro) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }

    await this.documentoRepositorio.actualizar(
      idEmpresa,
      {
        estado: ParametroEstado.INACTIVO,
      },
      usuarioAuditoria
    )
    return {
      id: idEmpresa,
      estado: ParametroEstado.INACTIVO,
    }
  }
}
