import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable, PreconditionFailedException } from '@nestjs/common'
import { CrearStandDto, StandRepository } from '../repository/stands.repository'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { ReservasDto } from '../dto/standsDto'
import { ColorStandsEstado, StandsEstado } from '../entities/stands.entity'
import PDFDocument from 'pdfkit'
import * as fs from 'fs' // Import fs to write the PDF file
import { DocumentoRepository } from '../repository/documento.repository'
import { EmailService } from 'src/application/mensajeria/service/mensajeria.service'
import { IngresosService } from './ingresos.service'
@Injectable()
export class StandService extends BaseService {
  constructor(
    @Inject(StandRepository)
    private standsRepositorio: StandRepository,
    private ingresoService: IngresosService,
    private documentoRepositorio: DocumentoRepository,
    private emailService: EmailService
  ) {
    super()
  }

  async listarPorBloques(paginacionQueryDto: PaginacionQueryDto) {
    return await this.standsRepositorio.listarPorBloques(paginacionQueryDto)
  }
  async crear(standDto: CrearStandDto, usuarioAuditoria: string) {
    return await this.standsRepositorio.crear(standDto, usuarioAuditoria)
  }

  async listarDetalle(id: string) {
    return await this.standsRepositorio.listarDetalle(id)
  }

  async listarPorPlanta(id: string) {
    return await this.standsRepositorio.listarPorPlanta(id)
  }

  async listarReservas(paginacionQueryDto: PaginacionQueryDto) {
    return await this.standsRepositorio.listarReservas(paginacionQueryDto)
  }

  async generarContrato(standDto: ReservasDto, usuarioAuditoria: string) {
    const { idStand, idEmpresa } = standDto
    const empresa = await this.standsRepositorio.BuscarEmpresaPorId(idEmpresa)
    const stand = await this.standsRepositorio.buscarPorId(idStand)
    // Generar contrato PDF utilizando Carbone
    const contratoPdfPath = await this.generarContratoPdf(stand, empresa)
    const nuevoPath = contratoPdfPath.replace('./contratos/', '')
    // Guardar la ruta del PDF en la base de datos
    await this.documentoRepositorio.crearDocumento(
      {
        idEmpresa: idEmpresa,
        documentoInvitacion: nuevoPath,
        descripcion: `Contrato de Reserva de Stand ${stand?.codigo}`,
        tipoDocumento: 'CONTRATO',
      },
      usuarioAuditoria
    )
    const datosCorreo = {
      correo: empresa?.correoEmpresa,
      nombres: empresa?.nombreEncargado,
      asunto: `Contrato de Reserva de Stand ${stand?.codigo}`,
    }
    await this.emailService.enviarContrato(datosCorreo, contratoPdfPath)
  }
  async reservar(standDto: ReservasDto, usuarioAuditoria: string) {
    const { idStand, monto } = standDto
    const stand = await this.standsRepositorio.buscarPorId(idStand)
    console.log('stand', standDto.descuento)
    if (standDto.descuento) {
      console.log('precio', stand?.precio)
      const nuevoPrecio = Number(stand?.precio) - Number(standDto.descuento)
      console.log('nuevoPrecio', nuevoPrecio)
      await this.standsRepositorio.actualizar(
        idStand,
        {
          precio: nuevoPrecio,
        },
        usuarioAuditoria
      )
    }
    const ingreso = await this.ingresoService.crear(
      {
        descripcion: `Reserva de stand ${stand?.codigo}`,
        tipo: 'INGRESO',
        monto: monto,
        idUsuario: usuarioAuditoria,
      },
      usuarioAuditoria
    )

    standDto.idIngreso = ingreso.id
    const respuesta = await this.standsRepositorio.reservar(
      standDto,
      usuarioAuditoria
    )
    const stands = new CrearStandDto()
    stands.estado = StandsEstado.RESERVADO
    stands.color = ColorStandsEstado[StandsEstado.RESERVADO]
    await this.standsRepositorio.actualizar(idStand, stands, usuarioAuditoria)

    return respuesta
  }
  async actualizar(
    id: string,
    standDto: CrearStandDto,
    usuarioAuditoria: string
  ) {
    return await this.standsRepositorio.actualizar(
      id,
      standDto,
      usuarioAuditoria
    )
  }

  async eliminarReserva(id: string, standDto: any, usuarioAuditoria: string) {
    const { idStand } = standDto
    const listartINgresoReserva =
      await this.standsRepositorio.listarReservaIngreso(id)
    const reserva = await this.standsRepositorio.eliminarReserva(id)
    if (listartINgresoReserva) {
      await this.standsRepositorio.eliminarIngresoReserva(
        listartINgresoReserva?.idIngreso ?? ''
      )
    }
    const stands = new CrearStandDto()
    stands.estado = StandsEstado.ACTIVO
    stands.color = ColorStandsEstado[StandsEstado.ACTIVO]
    await this.standsRepositorio.actualizar(idStand, stands, usuarioAuditoria)
    return reserva
  }
  async generarContratoPdf(stand: any, empresa: any): Promise<string> {
    return new Promise((resolve, reject) => {
      // Crear nuevo documento PDF
      const doc = new PDFDocument()

      // Definir la ruta donde se guardará el archivo PDF
      const filePath = `./contratos/contrato_stand_${stand.codigo}.pdf`

      // Crear carpeta si no existe
      if (!fs.existsSync('./contratos')) {
        fs.mkdirSync('./contratos')
      }

      // Crear stream de escritura para el archivo PDF
      const writeStream = fs.createWriteStream(filePath)
      doc.pipe(writeStream)

      // Información de ejemplo que incluirás en el contrato
      doc.fontSize(25).text('Contrato de Reserva de Stand', { align: 'center' })

      doc.moveDown()
      doc.fontSize(16).text(`Stand ID: ${stand.codigo}`)
      doc.text(`Empresa: ${empresa.nombreEmpresa}`)
      doc.text(`Rubro: ${empresa.rubroEmpresa}`)
      doc.text(`Dirección: ${empresa.direccionEmpresa}`)
      doc.text(`Teléfono: ${empresa.telefonoEmpresa}`)
      doc.text(`Correo: ${empresa.correoEmpresa}`)
      doc.text(`Descripción: ${empresa.descripcionEmpresa}`)
      doc.text(`NIT: ${empresa.nitEmpresa}`)
      doc.moveDown()
      doc.text(`Fecha: ${new Date().toLocaleDateString()}`)
      doc.text('Este contrato certifica la reserva del stand.')

      // Firma
      doc.moveDown()
      doc.text('_____________________________________', { align: 'right' })
      doc.text(`Firma de ${empresa.nombreEncargado}`, { align: 'right' })

      // Finalizar y cerrar el documento PDF
      doc.end()

      // Resolución del Promise una vez que el archivo esté escrito
      writeStream.on('finish', () => {
        resolve(filePath) // Devolver la ruta del PDF generado
      })

      writeStream.on('error', (error) => {
        reject(error) // Manejo de errores
      })
    })
  }
  async eliminarStand(id: string) {
    const buscarStand = await this.standsRepositorio.buscarPorId(id)
    if (!buscarStand) {
      throw new PreconditionFailedException('El stand no existe')
    }
    return await this.standsRepositorio.eliminarStand(id)
  }
}
