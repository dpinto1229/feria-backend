import { IsOptional } from 'class-validator'

export class ActualizarUsuarioDto {
  @IsOptional()
  estado?: string | null

  @IsOptional()
  correoElectronico?: string

  @IsOptional()
  contrasena?: string | null
}
