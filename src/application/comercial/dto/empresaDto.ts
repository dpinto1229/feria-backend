import { IsNotEmpty, IsOptional } from 'class-validator'

export class CrearActualizarEmpresaDto {
  @IsOptional()
  id?: string

  @IsNotEmpty()
  nombreEmpresa: string

  @IsNotEmpty()
  rubroEmpresa: string

  @IsNotEmpty()
  telefonoEmpresa: number

  @IsNotEmpty()
  correoEmpresa: string

  @IsNotEmpty()
  descripcionEmpresa: string

  @IsNotEmpty()
  nombreEncargado: string

  @IsNotEmpty()
  direccionEmpresa: string

  @IsNotEmpty()
  nitEmpresa: string

  evento: string

  @IsOptional()
  estado?: string
}
