import { Module } from '@nestjs/common'
import { MailerModule } from '@nestjs-modules/mailer'
import { EmailService } from './service/mensajeria.service'
@Module({
  providers: [EmailService],
  imports: [
    MailerModule.forRoot({
      transport: {
        host: process.env.MAIL_HOST,
        port: 587,
        secure: false,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      },
    }),
  ],
})
export class EmailModule {}
