import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Ingresos } from '../entities/ingresos.entity'
import { ActualizarIngresoDto, CrearIngresoDto } from '../dto/ingresosDto'

@Injectable()
export class IngresosRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Ingresos)
      .createQueryBuilder('ingresos')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    ingresoDto: ActualizarIngresoDto,
    usuarioAuditoria: string
  ) {
    const datosActualizar = new Ingresos({
      ...ingresoDto,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Ingresos)
      .update(id, datosActualizar)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro, orden, sentido } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Ingresos)
      .createQueryBuilder('ingresos')
      .leftJoinAndSelect('ingresos.usuario', 'usuario')
      .select([
        'ingresos.id',
        'ingresos.descripcion',
        'ingresos.tipo',
        'ingresos.monto',
        'ingresos.usuario',
        'ingresos.estado',
        'ingresos.fechaCreacion',
      ])
      .take(limite)
      .skip(saltar)
      .orderBy('ingresos.fechaCreacion', 'DESC')

    switch (orden) {
      case 'descripcion':
        query.addOrderBy('ingresos.descripcion', sentido)
        break
      case 'tipo':
        query.addOrderBy('ingresos.tipo', sentido)
        break

      default:
        query.orderBy('ingresos.id', 'ASC')
    }

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('ingresos.descripcion like :filtro', {
            filtro: `%${filtro}%`,
          }).orWhere('ingresos.tipo like :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }

  async crear(parametroDto: CrearIngresoDto, usuarioAuditoria: string) {
    const { descripcion, monto, tipo } = parametroDto

    const parametro = new Ingresos()
    parametro.descripcion = descripcion
    parametro.monto = monto
    parametro.tipo = tipo
    parametro.idUsuario = usuarioAuditoria
    parametro.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Ingresos).save(parametro)
  }

  async listarPresupuesto() {
    return await this.dataSource
      .getRepository(Ingresos)
      .createQueryBuilder('ingresos')
      .select([
        'ingresos.id',
        'ingresos.descripcion',
        'ingresos.monto',
        'ingresos.estado',
      ])
      .where('ingresos.tipo = :tipo', { tipo: 'INGRESO' })
      .getMany()
  }

  async listarEgresos() {
    return await this.dataSource
      .getRepository(Ingresos)
      .createQueryBuilder('ingresos')
      .select([
        'ingresos.id',
        'ingresos.descripcion',
        'ingresos.monto',
        'ingresos.estado',
      ])
      .where('ingresos.tipo = :tipo', { tipo: 'EGRESO' })
      .getMany()
  }
}
