import { MigrationInterface, QueryRunner } from 'typeorm'
import { USUARIO_SISTEMA } from '../../src/common/constants'
import { Bloques } from 'src/application/comercial/entities/bloques.entity'

export class insertsBloques1617820337610 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const items = [
      // APPS
      {
        nombre: 'Bloque Amarillo',
        descripcion: 'Bloque Amarillo',
        imagen:
          'https://www.ucpp.gob.bo/wp-content/uploads/2024/06/DSC_2925-scaled.jpg',
        _transaccion: 'SEEDS',
      },
      {
        nombre: 'Bloque Verde',
        descripcion: 'Bloque Verde',
        imagen: 'https://www.ucpp.gob.bo/wp-content/uploads/2024/07/v3.jpg',
        _transaccion: 'SEEDS',
      },
      // ACCIONES
      // FRONTEND
      {
        nombre: 'Bloque ROjo',
        descripcion: 'Bloque Rojo',
        imagen:
          'https://www.ucpp.gob.bo/wp-content/uploads/2024/06/DSC_2902-scaled.jpg',
        _transaccion: 'SEEDS',
      },
    ]
    const bloques = items.map((item) => {
      return new Bloques({
        nombre: item.nombre,
        descripcion: item.descripcion,
        imagen: item.imagen,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(bloques)
  }

  /* eslint-disable */
  public async down(queryRunner: QueryRunner): Promise<void> {}
}
