import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { Seguimiento } from '../entities/seguimiento.entity'
import { ParametroEstado } from 'src/application/parametro/constant'

@Injectable()
export class SeguimientoRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Seguimiento)
      .createQueryBuilder('seguimiento')
      .where({ id: id })
      .getOne()
  }

  //   async actualizar(id: string, documentoDto: any, usuarioAuditoria: string) {
  //     const datosActualizar = new Documento({
  //       ...documentoDto,
  //       usuarioModificacion: usuarioAuditoria,
  //     })
  //     return await this.dataSource
  //       .getRepository(Documento)
  //       .update(id, datosActualizar)
  //   }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Seguimiento)
      .createQueryBuilder('seguimiento')
      .leftJoinAndSelect('seguimiento.empresa', 'empresa')
      .select([
        'seguimiento.id',
        'seguimiento.tipo',
        'seguimiento.medio',
        'seguimiento.archivado',
        'seguimiento.observaciones',
        'seguimiento.estado',
        'seguimiento.fechaCreacion',
        'seguimiento.estadoEmpresa',
        'empresa.id',
        'empresa.nombreEmpresa',
        'empresa.descripcionEmpresa',
        'empresa.rubroEmpresa',
        'empresa.telefonoEmpresa',
        'empresa.nombreEncargado',
        'seguimiento.usuarioCreacion',
      ])
      .orderBy('seguimiento.id', 'ASC')
      .take(limite)
      .skip(saltar)

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('seguimiento.tipo like :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('seguimiento.medio ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('seguimiento.archivado ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('seguimiento.observaciones ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }
    return await query.getManyAndCount()
  }

  async crear(seguimientoDto: any, usuarioAuditoria: string) {
    const { tipo, medio, archivado, observaciones, idEmpresa, estado } =
      seguimientoDto

    const seguimiento = new Seguimiento()
    seguimiento.tipo = tipo
    seguimiento.medio = medio
    seguimiento.archivado = archivado
    seguimiento.observaciones = observaciones
    seguimiento.idEmpresa = idEmpresa
    seguimiento.usuarioCreacion = usuarioAuditoria
    seguimiento.estado = estado || ParametroEstado.ENVIADO

    return await this.dataSource.getRepository(Seguimiento).save(seguimiento)
  }

  async actualizar(id: string, seguimientoDto: any, usuarioAuditoria: string) {
    const {
      tipo,
      medio,
      archivado,
      observaciones,
      idEmpresa,
      estado,
      estadoEmpresa,
    } = seguimientoDto

    const query = this.dataSource
      .getRepository(Seguimiento)
      .createQueryBuilder('seguimiento')
      .update()
      .set({
        tipo,
        medio,
        archivado,
        observaciones,
        idEmpresa,
        estado,
        estadoEmpresa,
        usuarioModificacion: usuarioAuditoria,
      })
      .where('id = :id', { id })
      .execute()
    return query
  }
}
