import { BaseService } from '../../../common/base'
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common'
import { UsuarioService } from '../../usuario/service/usuario.service'
import { JwtService } from '@nestjs/jwt'
import { TextService } from '../../../common/lib/text.service'
import { RefreshTokensService } from './refreshTokens.service'
import {
  Status,
  USUARIO_NORMAL,
  USUARIO_SISTEMA,
} from '../../../common/constants'
import { Messages } from '../../../common/constants/response-messages'
import { PersonaDto } from '../../usuario/dto/persona.dto'
import { ConfigService } from '@nestjs/config'
import { PersonaService } from '../../usuario/service/persona.service'
import { CambioRolDto } from '../dto/index.dto'

@Injectable()
export class AuthenticationService extends BaseService {
  // eslint-disable-next-line max-params
  constructor(
    private readonly personaService: PersonaService,
    private readonly usuarioService: UsuarioService,
    private readonly jwtService: JwtService,
    private readonly refreshTokensService: RefreshTokensService,
    @Inject(ConfigService) private readonly configService: ConfigService
  ) {
    super()
  }

  async validarUsuario(usuario: string, contrasena: string) {
    const respuesta = await this.usuarioService.buscarUsuario(usuario)

    if (!respuesta) {
      return null
    }

    if (respuesta?.usuarioRol.length === 0) {
      throw new UnauthorizedException(Messages.NO_PERMISSION_USER)
    }

    if (respuesta?.estado === Status.PENDING) {
      throw new UnauthorizedException(Messages.PENDING_USER)
    }

    if (respuesta?.estado === Status.INACTIVE) {
      throw new UnauthorizedException(Messages.INACTIVE_USER)
    }

    const pass = TextService.decodeBase64(contrasena)

    if (!(await TextService.compare(pass, respuesta.contrasena))) {
      throw new UnauthorizedException(Messages.INVALID_USER_CREDENTIALS)
    }

    return {
      id: respuesta.id,
      roles: respuesta.usuarioRol
        .filter((usuarioRol) => usuarioRol.estado === Status.ACTIVE)
        .map((usuarioRol) => usuarioRol.rol.rol),
    }
  }

  async autenticar(user: PassportUser) {
    const usuario = await this.usuarioService.buscarUsuarioId(user.id)

    const rol = this.usuarioService.obtenerRolActual(usuario.roles, user.idRol)

    const payload: PayloadType = {
      id: user.id,
      roles: user.roles,
      idRol: rol.idRol,
      rol: rol.rol,
    }
    // crear refresh_token
    const refreshToken = await this.refreshTokensService.create(user.id)
    // construir respuesta
    const data = {
      access_token: this.jwtService.sign(payload),
      ...usuario,
      idRol: rol.idRol,
      rol: rol.rol,
    }
    return {
      refresh_token: { id: refreshToken.id },
      data,
    }
  }

  async cambiarRol(user: PassportUser, data: CambioRolDto) {
    const usuarioRol = {
      ...user,
      idRol: data.idRol,
    }

    return await this.autenticar(usuarioRol)
  }

  async validarUsuarioOidc(persona: PersonaDto) {
    const respuesta = await this.usuarioService.buscarUsuarioPorCI(persona)

    if (!respuesta) {
      return null
    }

    if (respuesta?.usuarioRol.length === 0) {
      throw new UnauthorizedException(Messages.NO_PERMISSION_USER)
    }

    const { persona: datosPersona } = respuesta

    if (respuesta.estado === Status.INACTIVE) {
      throw new UnauthorizedException(Messages.INACTIVE_USER)
    }

    // actualizar datos persona
    if (
      datosPersona.nombres !== persona.nombres ||
      datosPersona.primerApellido !== persona.primerApellido ||
      datosPersona.segundoApellido !== persona.segundoApellido
    ) {
      await this.usuarioService.actualizarDatosPersona(persona)
    }

    return {
      id: respuesta.id,
      roles: respuesta.usuarioRol.map((usuarioRol) => usuarioRol.rol.rol),
    }
  }

  async validarOCrearUsuarioOidc(
    persona: PersonaDto,
    datosUsuario: {
      correoElectronico: string
    }
  ) {
    const respuesta = await this.usuarioService.buscarUsuarioPorCI(persona)

    if (!respuesta) {
      // No existe persona, o no cuenta con un usuario - registrar

      const respPersona = await this.personaService.buscarPersonaPorCI(persona)

      if (!respPersona) {
        // No existe la persona en base de datos, crear registro completo de persona

        const respuesta = await this.usuarioService.buscarUsuarioPorCI(persona)

        if (!respuesta) {
          return null
        }

        return {
          id: respuesta.id,
          roles: respuesta.usuarioRol.map((usuarioRol) => usuarioRol.rol.rol),
        }
      }

      // Persona existe en base de datos, solo crear usuario
      if (respPersona.estado === Status.INACTIVE) {
        throw new UnauthorizedException(Messages.INACTIVE_PERSON)
      }

      // Actualizar datos persona
      if (
        respPersona.nombres !== persona.nombres ||
        respPersona.primerApellido !== persona.primerApellido ||
        respPersona.segundoApellido !== persona.segundoApellido
      ) {
        await this.usuarioService.actualizarDatosPersona(persona)
      }
      // Crear usuario y rol
      await this.usuarioService.crearConPersonaExistente(
        respPersona.id,
        respPersona.nroDocumento,
        datosUsuario,
        USUARIO_NORMAL
      )

      const respuesta = await this.usuarioService.buscarUsuarioPorCI(persona)

      if (!respuesta) {
        return null
      }

      return {
        id: respuesta.id,
        roles: respuesta.usuarioRol.map((usuarioRol) => usuarioRol.rol.rol),
      }
    }

    // Persona y usuario existen en BD

    const { estado, persona: datosPersona } = respuesta

    if (estado === Status.INACTIVE) {
      throw new UnauthorizedException(Messages.INACTIVE_USER)
    }

    if (
      datosPersona.nombres !== persona.nombres ||
      datosPersona.primerApellido !== persona.primerApellido ||
      datosPersona.segundoApellido !== persona.segundoApellido
    ) {
      // Actualizar datos de persona
      await this.usuarioService.actualizarDatosPersona(persona)
    }

    if (datosUsuario.correoElectronico !== respuesta.correoElectronico) {
      // Actualizar correo si es diferente en ciudadanía
      await this.usuarioService.actualizarDatos(
        respuesta.id,
        {
          correoElectronico: datosUsuario.correoElectronico,
          roles: respuesta.usuarioRol.map((value) => value.rol.id),
        },
        USUARIO_SISTEMA
      )
    }
    return {
      id: respuesta.id,
      roles: respuesta.usuarioRol.map((usuarioRol) => usuarioRol.rol.rol),
    }
  }

  async autenticarOidc(user: PassportUser) {
    const usuario = await this.usuarioService.buscarUsuarioId(user.id)

    const rol = this.usuarioService.obtenerRolActual(usuario.roles, user.idRol)

    const payload: PayloadType = {
      id: user.id,
      roles: user.roles,
      idRol: rol.idRol,
      rol: rol.rol,
    }
    // crear refresh_token
    const refreshToken = await this.refreshTokensService.create(user.id)
    // construir respuesta
    const data = {
      access_token: this.jwtService.sign(payload),
    }
    return {
      refresh_token: { id: refreshToken.id },
      data,
    }
  }
}
