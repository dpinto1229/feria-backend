import { BaseService } from '../../../common/base'
import {
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
  PreconditionFailedException,
  Query,
  UnauthorizedException,
} from '@nestjs/common'
import { UsuarioRepository } from '../repository/usuario.repository'
import {
  Status,
  TipoDocumento,
  USUARIO_NORMAL,
} from '../../../common/constants'
import { CrearUsuarioDto } from '../dto/crear-usuario.dto'
import { TextService } from '../../../common/lib/text.service'
import { Messages } from '../../../common/constants/response-messages'
import { AuthorizationService } from '../../authorization/controller/authorization.service'
import { PersonaDto } from '../dto/persona.dto'
import { UsuarioRolRepository } from '../../authorization/repository/usuario-rol.repository'
import { ActualizarUsuarioRolDto } from '../dto/actualizar-usuario-rol.dto'
import { ConfigService } from '@nestjs/config'
import { FiltrosUsuarioDto } from '../dto/filtros-usuario.dto'
import { RolRepository } from '../../authorization/repository/rol.repository'
import { EntityManager, QueryRunner } from 'typeorm'
import { CrearUsuarioCuentaDto } from '../dto/crear-usuario-cuenta.dto'
import { PersonaRepository } from '../repository/persona.repository'
import { EmailService } from '../../../../src/application/mensajeria/service/mensajeria.service'

@Injectable()
export class UsuarioService extends BaseService {
  constructor(
    @Inject(UsuarioRepository)
    private usuarioRepositorio: UsuarioRepository,
    @Inject(UsuarioRolRepository)
    private usuarioRolRepositorio: UsuarioRolRepository,
    @Inject(RolRepository)
    private rolRepositorio: RolRepository,
    @Inject(PersonaRepository)
    private personaRepositorio: PersonaRepository,
    @Inject(EmailService)
    private emailService: EmailService,
    private readonly authorizationService: AuthorizationService,
    private configService: ConfigService
  ) {
    super()
  }

  async listar(@Query() paginacionQueryDto: FiltrosUsuarioDto) {
    return await this.usuarioRepositorio.listar(paginacionQueryDto)
  }

  async buscarUsuario(usuario: string) {
    return await this.usuarioRepositorio.buscarUsuario(usuario)
  }

  async crear(usuarioDto: CrearUsuarioDto, usuarioAuditoria: string) {
    // verificar si el usuario ya fue registrado
    const usuario = await this.usuarioRepositorio.buscarUsuarioPorCI(
      usuarioDto.persona
    )

    if (usuario) {
      throw new PreconditionFailedException(Messages.EXISTING_USER)
    }

    // verificar si el correo no esta registrado
    const correo = await this.usuarioRepositorio.buscarUsuarioPorCorreo(
      usuarioDto.correoElectronico
    )

    if (correo) {
      throw new PreconditionFailedException(Messages.EXISTING_EMAIL)
    }

    const { roles } = usuarioDto

    const contrasena = TextService.generateShortRandomText()

    const datosCorreo = {
      correo: usuarioDto.correoElectronico,
      nombres: usuarioDto.persona.nombres,
      usuario: usuarioDto.persona.nroDocumento,
      asunto: Messages.SUBJECT_EMAIL_ACCOUNT_ACTIVE,
    }

    const op = async (transaction: EntityManager) => {
      usuarioDto.contrasena = await TextService.encrypt(contrasena)
      usuarioDto.estado = Status.ACTIVE

      const persona = await this.personaRepositorio.crear(
        usuarioDto.persona,
        usuarioAuditoria,
        transaction
      )

      const usuario = await this.usuarioRepositorio.crear(
        persona.id,
        {
          ...usuarioDto,
          usuario: usuarioDto.usuario ?? usuarioDto?.persona?.nroDocumento,
        },
        usuarioAuditoria,
        transaction
      )

      await this.usuarioRolRepositorio.crear(
        usuario.id,
        roles,
        usuarioAuditoria,
        transaction
      )

      return usuario
    }

    const crearResult = await this.usuarioRepositorio.runTransaction(op)
    await this.emailService.enviarEMail(contrasena, datosCorreo)
    return crearResult
  }

  async crearCuenta(usuarioDto: CrearUsuarioCuentaDto) {
    // verificar si el usuario ya fue registrado con su correo
    const usuario = await this.usuarioRepositorio.buscarUsuario(
      usuarioDto.correoElectronico
    )

    if (usuario) {
      throw new PreconditionFailedException(Messages.EXISTING_EMAIL)
    }

    // verificar si el correo no esta registrado
    const correo = await this.usuarioRepositorio.buscarUsuarioPorCorreo(
      usuarioDto.correoElectronico
    )

    if (correo) {
      throw new PreconditionFailedException(Messages.EXISTING_EMAIL)
    }

    const rol = await this.rolRepositorio.buscarPorNombreRol('USUARIO')

    if (!rol) {
      throw new PreconditionFailedException(Messages.NO_PERMISSION_FOUND)
    }

    if (!TextService.validateLevelPassword(usuarioDto.contrasenaNueva)) {
      throw new PreconditionFailedException(Messages.INVALID_PASSWORD_SCORE)
    }

    const op = async (transaction: EntityManager) => {
      const personaNueva = await this.personaRepositorio.crear(
        {
          nombres: usuarioDto.nombres,
          primerApellido: '',
          segundoApellido: '',
          nroDocumento: TextService.textToUuid(usuarioDto.correoElectronico),
          tipoDocumento: TipoDocumento.OTRO,
        },
        USUARIO_NORMAL,
        transaction
      )

      const usuarioNuevo = await this.usuarioRepositorio.crear(
        personaNueva.id,
        {
          usuario: usuarioDto.correoElectronico,
          correoElectronico: usuarioDto.correoElectronico,
          estado: Status.PENDING,
          contrasena: await TextService.encrypt(usuarioDto.contrasenaNueva),
        },
        USUARIO_NORMAL,
        transaction
      )

      await this.usuarioRolRepositorio.crear(
        usuarioNuevo.id,
        [rol.id],
        USUARIO_NORMAL,
        transaction
      )
      return usuarioNuevo
    }
    return await this.usuarioRepositorio.runTransaction(op)
  }

  async crearConPersonaExistente(
    idPersona: string,
    nroDocumento: string,
    otrosDatos: { correoElectronico: string },
    usuarioAuditoria: string
  ) {
    const op = async (transaction: EntityManager) => {
      // verificar si el usuario ya fue registrado
      const usuario = await this.usuarioRepositorio.verificarExisteUsuarioPorCI(
        nroDocumento,
        transaction
      )

      if (usuario) {
        throw new PreconditionFailedException(Messages.EXISTING_USER)
      }

      const usuarioResult = await this.usuarioRepositorio.crear(
        idPersona,
        {
          estado: Status.ACTIVE,
          correoElectronico: otrosDatos?.correoElectronico,
        },
        usuarioAuditoria,
        transaction
      )

      const rol = await this.rolRepositorio.buscarPorNombreRol(
        'USUARIO',
        transaction
      )

      if (!rol) {
        throw new NotFoundException(Messages.NO_PERMISSION_FOUND)
      }

      await this.usuarioRolRepositorio.crear(
        usuarioResult.id,
        [rol.id],
        usuarioAuditoria,
        transaction
      )

      return usuarioResult
    }

    const result = await this.usuarioRepositorio.runTransaction(op)

    return { id: result.id, estado: result.estado }
  }

  async activar(idUsuario: string, usuarioAuditoria: string) {
    this.verificarPermisos(idUsuario, usuarioAuditoria)
    const usuario = await this.usuarioRepositorio.buscarPorId(idUsuario)
    const statusValid = [Status.CREATE, Status.INACTIVE, Status.PENDING]

    if (!(usuario && statusValid.includes(usuario.estado as Status))) {
      throw new NotFoundException(Messages.INVALID_USER)
    }

    // cambiar estado al usuario y generar una nueva contrasena
    const contrasena = TextService.generateShortRandomText()

    await this.usuarioRepositorio.actualizar(
      idUsuario,
      {
        contrasena: await TextService.encrypt(contrasena),
        estado: Status.ACTIVE,
      },
      usuarioAuditoria
    )

    const usuarioActualizado = await this.usuarioRepositorio.buscarPorId(
      usuario.id
    )

    if (!usuarioActualizado) {
      throw new PreconditionFailedException(Messages.INVALID_USER)
    }

    return { id: usuarioActualizado.id, estado: usuarioActualizado.estado }
  }

  async inactivar(idUsuario: string, usuarioAuditoria: string) {
    this.verificarPermisos(idUsuario, usuarioAuditoria)
    const usuario = await this.usuarioRepositorio.buscarPorId(idUsuario)

    if (!usuario) {
      throw new NotFoundException(Messages.INVALID_USER)
    }

    await this.usuarioRepositorio.actualizar(
      idUsuario,
      {
        estado: Status.INACTIVE,
      },

      usuarioAuditoria
    )

    const usuarioActualizado = await this.usuarioRepositorio.buscarPorId(
      usuario.id
    )

    if (!usuarioActualizado) {
      throw new NotFoundException(Messages.INVALID_USER)
    }

    return {
      id: usuarioActualizado.id,
      estado: usuarioActualizado.estado,
    }
  }

  verificarPermisos(usuarioAuditoria: string, id: string) {
    if (usuarioAuditoria === id) {
      throw new ForbiddenException(Messages.EXCEPTION_OWN_ACCOUNT_ACTION)
    }
  }

  async actualizarContrasena(
    idUsuario: string,
    contrasenaActual: string,
    contrasenaNueva: string
  ) {
    const hash = TextService.decodeBase64(contrasenaActual)
    const usuario =
      await this.usuarioRepositorio.buscarUsuarioRolPorId(idUsuario)

    if (!(usuario && (await TextService.compare(hash, usuario.contrasena)))) {
      throw new PreconditionFailedException(Messages.INVALID_CREDENTIALS)
    }
    // validar que la contraseña nueva cumpla nivel de seguridad
    const contrasena = TextService.decodeBase64(contrasenaNueva)

    if (!TextService.validateLevelPassword(contrasena)) {
      throw new PreconditionFailedException(Messages.INVALID_PASSWORD_SCORE)
    }

    // guardar en bd
    await this.usuarioRepositorio.actualizar(
      idUsuario,
      {
        contrasena: await TextService.encrypt(contrasena),
        estado: Status.ACTIVE,
      },
      idUsuario
    )

    const usuarioActualizado = await this.usuarioRepositorio.buscarPorId(
      usuario.id
    )

    if (!usuarioActualizado) {
      throw new PreconditionFailedException(Messages.INVALID_USER)
    }

    return {
      id: usuarioActualizado.id,
      estado: usuarioActualizado.estado,
    }
  }

  async actualizarDatos(
    id: string,
    usuarioDto: ActualizarUsuarioRolDto,
    usuarioAuditoria: string
  ) {
    this.verificarPermisos(id, usuarioAuditoria)
    // 1. verificar que exista el usuario
    const op = async (transaction: EntityManager) => {
      const usuario = await this.usuarioRepositorio.buscarPorId(id, transaction)

      if (!usuario) {
        throw new NotFoundException(Messages.INVALID_USER)
      }

      const { persona } = usuarioDto

      if (persona) {
        const personaResult = await this.personaRepositorio.buscarPersonaId(
          usuario.idPersona,
          transaction
        )
        if (!personaResult) {
          throw new PreconditionFailedException(Messages.INVALID_USER)
        }

        await this.usuarioRepositorio.ActualizarDatosPersonaId(
          personaResult.id,
          persona,
          transaction
        )
      }

      const { correoElectronico, roles } = usuarioDto
      // 2. verificar que el email no este registrado

      if (
        correoElectronico &&
        correoElectronico !== usuario.correoElectronico
      ) {
        const existe = await this.usuarioRepositorio.buscarUsuarioPorCorreo(
          correoElectronico,
          transaction
        )
        if (existe) {
          throw new PreconditionFailedException(Messages.EXISTING_EMAIL)
        }
        await this.usuarioRepositorio.actualizar(
          id,
          {
            correoElectronico: correoElectronico,
          },
          usuarioAuditoria,
          transaction
        )
      }

      if (roles.length > 0) {
        // realizar reglas de roles
        await this.actualizarRoles(id, roles, usuarioAuditoria, transaction)
      }

      return { id: usuario.id }
    }

    const usuarioResult = await this.usuarioRepositorio.runTransaction(op)

    return { id: usuarioResult.id }
  }

  async actualizarRoles(
    id: string,
    roles: Array<string>,
    usuarioAuditoria: string,
    transaccion?: EntityManager
  ) {
    const usuarioRoles =
      await this.usuarioRolRepositorio.obtenerRolesPorUsuario(id, transaccion)

    const { inactivos, activos, nuevos } = this.verificarUsuarioRoles(
      usuarioRoles,
      roles
    )

    // ACTIVAR roles inactivos
    if (inactivos.length > 0) {
      await this.usuarioRolRepositorio.activar(
        id,
        inactivos,
        usuarioAuditoria,
        transaccion
      )
    }
    // INACTIVAR roles activos
    if (activos.length > 0) {
      await this.usuarioRolRepositorio.inactivar(
        id,
        activos,
        usuarioAuditoria,
        transaccion
      )
    }
    // CREAR nuevos roles
    if (nuevos.length > 0) {
      await this.usuarioRolRepositorio.crear(
        id,
        nuevos,
        usuarioAuditoria,
        transaccion
      )
    }
  }

  verificarUsuarioRoles(
    usuarioRoles: Array<{
      rol: { id: string }
      estado: string
    }>,
    roles: Array<string>
  ) {
    const inactivos = roles.filter((rol) =>
      usuarioRoles.some(
        (usuarioRol) =>
          usuarioRol.rol.id === rol && usuarioRol.estado === Status.INACTIVE
      )
    )

    const activos = usuarioRoles
      .filter(
        (usuarioRol) =>
          !roles.includes(usuarioRol.rol.id) &&
          usuarioRol.estado === Status.ACTIVE
      )
      .map((usuarioRol) => usuarioRol.rol.id)

    const nuevos = roles.filter((rol) =>
      usuarioRoles.every((usuarioRol) => usuarioRol.rol.id !== rol)
    )

    return {
      activos,
      inactivos,
      nuevos,
    }
  }

  async buscarUsuarioPerfil(id: string, idRol: string) {
    const perfil = await this.buscarUsuarioId(id)
    return { ...perfil, idRol }
  }

  async buscarUsuarioId(id: string) {
    const usuario = await this.usuarioRepositorio.buscarUsuarioRolPorId(id)

    if (!usuario) {
      throw new NotFoundException(Messages.INVALID_USER)
    }

    return {
      id: usuario.id,
      usuario: usuario.usuario,
      estado: usuario.estado,
      avatar: usuario.avatar,
      contrasena: usuario.contrasena,
      roles: await Promise.all(
        usuario.usuarioRol
          .filter((value) => value.estado === Status.ACTIVE)
          .map(async (usuarioRol) => {
            const { id, rol, nombre } = usuarioRol.rol
            const modulos =
              await this.authorizationService.obtenerPermisosPorRol(rol)
            return {
              idRol: id,
              rol,
              nombre,
              modulos,
            }
          })
      ),
      persona: usuario.persona,
    }
  }

  async buscarUsuarioPorCI(persona: PersonaDto) {
    return await this.usuarioRepositorio.buscarUsuarioPorCI(persona)
  }

  async actualizarDatosPersona(datosPersona: PersonaDto) {
    return await this.usuarioRepositorio.actualizarDatosPersona(datosPersona)
  }

  obtenerRolActual(
    roles: Array<{ idRol: string; rol: string }>,
    idRol: string | null | undefined
  ) {
    if (roles.length < 1) {
      throw new UnauthorizedException(`El usuario no cuenta con roles.`)
    }

    // buscar el primer rol
    if (!idRol) {
      return roles[0]
    }

    // buscar el rol activo
    const rol = roles.find((item) => item.idRol === idRol)
    if (!rol) {
      throw new UnauthorizedException(`Rol no permitido.`)
    }
    return rol
  }
  async actualizarPerfil(id: string, perfilDto: any, usuarioAuditoria: string) {
    const op = async (transaction: EntityManager) => {
      const usuario = await this.buscarUsuarioPorId(id)
      if (!usuario) {
        throw new NotFoundException(Messages.INVALID_USER)
      }
      await this.usuarioRepositorio.actualizarPerfil(
        id,
        perfilDto,
        usuarioAuditoria,
        transaction
      )
    }
    // return { id }
    return await this.usuarioRepositorio.runTransaction(op)
  }
  async buscarUsuarioPorId(idUsuario: string) {
    return this.usuarioRepositorio.buscarPorId(idUsuario)
  }

  async validarContrasena(idUsuario: string, contrasena: string) {
    const usuario = await this.usuarioRepositorio.buscarPorId(idUsuario)
    if (!usuario) {
      throw new NotFoundException(Messages.INVALID_USER)
    }

    const hash = TextService.decodeBase64(contrasena)
    if (!(await TextService.compare(hash, usuario.contrasena))) {
      throw new PreconditionFailedException(Messages.INVALID_CREDENTIALS)
    }
    return true
  }
}
