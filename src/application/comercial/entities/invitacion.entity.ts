import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Empresas } from './empresas.entity'

dotenv.config()

export enum TipoDocumento {
  INVITACION = 'INVITACION',
  CONTRATO = 'CONTRATO',
}
@Check(UtilService.buildStatusCheck(ParametroEstado))
@Check(UtilService.buildCheck('tipo_documento', TipoDocumento))
@Entity({ name: 'documento_invitacion', schema: process.env.DB_SCHEMA_FERIA })
export class Documento extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 150,
    type: 'varchar',
    name: 'documento_invitacion',
    comment: 'Documento de la invitación',
  })
  documentoInvitacion: string

  @Column({
    length: 255,
    type: 'varchar',
    name: 'descripcion',
    comment: 'Nombre del encargado de la empresa',
  })
  descripcion: string

  //tipo de documento
  @Column({
    length: 255,
    type: 'varchar',
    name: 'tipo_documento',
    comment: 'Tipo de documento',
  })
  tipoDocumento: string

  @Column({
    name: 'id_empresa',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla de Personas',
  })
  idEmpresa: string

  @ManyToOne(() => Empresas, (empresa) => empresa.documento, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_empresa',
    referencedColumnName: 'id',
  })
  empresa: Empresas

  constructor(data?: Partial<Documento>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ParametroEstado.ACTIVO
  }
}
