import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ParametroEstado } from 'src/application/parametro/constant'
import { Documento } from './invitacion.entity'
import { Seguimiento } from './seguimiento.entity'
import { Reservas } from './reserva.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(ParametroEstado))
@Entity({ name: 'empresas', schema: process.env.DB_SCHEMA_FERIA })
export class Empresas extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 150,
    type: 'varchar',
    name: 'nombre_empresa',
    comment: 'Nombre de la empresa',
  })
  nombreEmpresa: string

  @Column({
    length: 150,
    type: 'varchar',
    comment: 'Rubro de la empresa',
    name: 'rubro_empresa',
  })
  rubroEmpresa: string

  @Column({
    type: 'integer',
    default: 0,
    name: 'telefono_empresa',
    comment: 'Teléfono de la empresa',
  })
  telefonoEmpresa: number

  @Column({
    length: 150,
    type: 'varchar',
    comment: 'Correo de la empresa',
    name: 'correo_empresa',
  })
  correoEmpresa: string

  @Column({
    length: 150,
    type: 'varchar',
    comment: 'Descripcion de la empresa',
    name: 'descripcion_empresa',
  })
  descripcionEmpresa: string

  @Column({
    length: 150,
    type: 'varchar',
    comment: 'Encargado de la empresa',
    name: 'nombre_encargado',
  })
  nombreEncargado: string

  @Column({
    length: 150,
    type: 'varchar',
    comment: 'Direccion de la empresa',
    name: 'direccion_empresa',
  })
  direccionEmpresa: string

  @Column({
    length: 150,
    type: 'varchar',
    comment: 'NIT de la empresa',
    name: 'nit_empresa',
  })
  nitEmpresa: string

  @Column({
    length: 250,
    type: 'varchar',
    nullable: true,
    comment: 'Estado de la empresa',
    name: 'evento',
  })
  evento: string

  @OneToMany(() => Documento, (documento) => documento.empresa)
  documento: Documento[]

  @OneToMany(() => Seguimiento, (seguimiento) => seguimiento.empresa)
  seguimiento: Seguimiento[]

  @OneToMany(() => Reservas, (reservas) => reservas.empresa)
  reservas: Reservas[]
  constructor(data?: Partial<Empresas>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ParametroEstado.ACTIVO
  }
}
