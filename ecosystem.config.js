module.exports = {
  apps: [
    {
      name: 'dispo-backend',
      script: './dist/src/main.js',
      instances: 1,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
}
