import { Controller, Get, UseGuards } from '@nestjs/common'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { BaseController } from '../../../common/base'
import { DashboardService } from '../service/dashboard.service'

@Controller('dashboard')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class DashboardController extends BaseController {
  constructor(private dashboardService: DashboardService) {
    super()
  }

  @Get('/cards')
  async listar() {
    const result = await this.dashboardService.cards()
    return this.success(result)
  }

  @Get('/charts')
  async listartCharts() {
    const result = await this.dashboardService.charts()
    return this.success(result)
  }
}
