import { IsNumberString } from '../validation'

export class ParamIdDto {
  @IsNumberString()
  id: string
}
export class ParamNombreDto {
  nombre: string
}
